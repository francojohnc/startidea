import React, {Component} from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import {DropdownContext} from "./DropdownContext";
import omit from "lib/utils/omit";

const propTypes = {
    children: PropTypes.node,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    divider: PropTypes.bool,
    as: PropTypes.elementType,
    header: PropTypes.bool,
    onClick: PropTypes.func,
    className: PropTypes.string,
    toggle: PropTypes.bool
};

const defaultProps = {
    as: "button",
    toggle: false
};

export default class DropdownItem extends Component {
    static contextType = DropdownContext;
    static propTypes = propTypes;
    static defaultProps = defaultProps;

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.getTabIndex = this.getTabIndex.bind(this);
    }

    onClick(e) {
        if (this.props.disabled || this.props.header || this.props.divider) {
            e.preventDefault();
            return;
        }

        if (this.props.onClick) {
            this.props.onClick(e);
        }

        if (this.props.toggle) {
            this.context.toggle(e);
        }
    }

    getTabIndex() {
        if (this.props.disabled || this.props.header || this.props.divider) {
            return "-1";
        }

        return "0";
    }

    render() {
        const tabIndex = this.getTabIndex();
        const role = tabIndex > -1 ? "menuitem" : undefined;
        let {
            className,
            divider,
            as: Tag,
            header,
            active,
            children,
            content,
            ...props
        } = omit(["toggle"], this.props);

        const classes = cx(className, {
            disabled: props.disabled,
            "dropdown-item": !divider && !header,
            active: active,
            "dropdown-header": header,
            "dropdown-divider": divider
        });

        if (Tag === "button") {
            if (header) {
                Tag = "h5";
            } else if (divider) {
                Tag = "div";
            } else if (props.href) {
                Tag = "a";
            }
        }

        return (
            <Tag
                type={
                    Tag === "button" && (props.onClick || this.props.toggle)
                        ? "button"
                        : undefined
                }
                {...props}
                tabIndex={tabIndex}
                role={role}
                className={classes}
                onClick={this.onClick}
            >
                {children || content}
            </Tag>
        );
    }
}
