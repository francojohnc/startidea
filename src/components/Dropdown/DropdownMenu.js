import React, { Component } from "react";
import PropTypes from "prop-types";
import { Popper } from "react-popper";
import cx from "lib/dom/classnames";
import { DropdownContext } from "./DropdownContext";

const propTypes = {
  as: PropTypes.elementType,
  children: PropTypes.node.isRequired,
  right: PropTypes.bool,
  flip: PropTypes.bool,
  modifiers: PropTypes.object,
  className: PropTypes.string,
  persist: PropTypes.bool,
  positionFixed: PropTypes.bool
};

const defaultProps = {
  as: "div",
  flip: true
};

const noFlipModifier = { flip: { enabled: false } };

const directionPositionMap = {
  up: "top",
  left: "left",
  right: "right",
  down: "bottom"
};

export default class DropdownMenu extends Component {
  static contextType = DropdownContext;

  static propTypes = propTypes;
  static defaultProps = defaultProps;

  render() {
    const {
      className,
      right,
      as: Tag,
      flip,
      modifiers,
      persist,
      positionFixed,
      ...attrs
    } = this.props;

    const classes = cx(className, "dropdown-menu", {
      "dropdown-menu-right": right,
      show: this.context.isOpen
    });

    if (persist || (this.context.isOpen && !this.context.inNavbar)) {
      const position1 =
        directionPositionMap[this.context.direction] || "bottom";
      const position2 = right ? "end" : "start";
      const poperPlacement = `${position1}-${position2}`;
      const poperModifiers = !flip
        ? {
            ...modifiers,
            ...noFlipModifier
          }
        : modifiers;
      const popperPositionFixed = !!positionFixed;

      return (
        <Popper
          placement={poperPlacement}
          modifiers={poperModifiers}
          positionFixed={popperPositionFixed}
        >
          {({ ref, style, placement }) => (
            <Tag
              tabIndex="-1"
              role="menu"
              ref={ref}
              style={style}
              {...attrs}
              aria-hidden={!this.context.isOpen}
              className={classes}
              x-placement={placement}
            />
          )}
        </Popper>
      );
    }

    return (
      <Tag
        tabIndex="-1"
        role="menu"
        {...attrs}
        aria-hidden={!this.context.isOpen}
        className={classes}
        x-placement={attrs.placement}
      />
    );
  }
}
