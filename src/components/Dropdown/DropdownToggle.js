import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import { Reference } from "react-popper";
import Button from "components/Button";
import { DropdownContext } from "./DropdownContext";
import "./style.min.css";

const propTypes = {
  as: PropTypes.elementType,
  caret: PropTypes.bool,
  variant: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  "aria-haspopup": PropTypes.bool,
  split: PropTypes.bool
};

const defaultProps = {
  "aria-haspopup": true,
  variant: "secondary"
};

export default class DropdownToggle extends Component {
  static contextType = DropdownContext;
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    if (this.props.disabled || this.context.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.nav && !this.props.tag) {
      e.preventDefault();
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }

    this.context.toggle(e);
  }

  render() {
    const { className, color, caret, split, as, ...props } = this.props;

    const ariaLabel = props["aria-label"] || "Toggle Dropdown";
    const classes = cx(className, {
      "dropdown-toggle": caret || split,
      "dropdown-toggle-split": split
    });

    const children = props.children || (
      <span className="sr-only">{ariaLabel}</span>
    );

    let Tag = as;

    if (!as) {
      Tag = Button;
      props.color = color;
    }

    return (
      <Reference>
        {({ ref }) => (
          <Tag
            {...props}
            {...{ [typeof Tag === "string" ? "ref" : "innerRef"]: ref }}
            className={classes}
            onClick={this.onClick}
            aria-expanded={this.context.isOpen}
            children={children}
          />
        )}
      </Reference>
    );
  }
}
