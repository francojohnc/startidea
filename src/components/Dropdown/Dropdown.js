import React, { Component, createRef } from "react";
import PropTypes from "prop-types";
import { Manager } from "react-popper";
import cx from "lib/dom/classnames";
import omit from "lib/utils/omit";
import isFalsy from "lib/utils/isFalsy";
import { DropdownContext } from "./DropdownContext";

const propTypes = {
  a11y: PropTypes.bool,
  disabled: PropTypes.bool,
  direction: PropTypes.oneOf(["up", "down", "left", "right"]),
  group: PropTypes.bool,
  isOpen: PropTypes.bool,
  active: PropTypes.bool,
  addonType: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.oneOf(["prepend", "append"])
  ]),
  size: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["lg", "sm"])]),
  as: PropTypes.elementType,
  toggle: PropTypes.func,
  children: PropTypes.node,
  className: PropTypes.string,
  manual: PropTypes.bool
};

const defaultProps = {
  a11y: true,
  disabled: false,
  isOpen: false,
  direction: "down",
  active: false,
  addonType: false,
  manual: false
};

export default class Dropdown extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this.containerRef = createRef();

    this.addEvents = this.addEvents.bind(this);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.removeEvents = this.removeEvents.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this.handleProps();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isOpen !== prevProps.isOpen ||
      this.state.isOpen !== prevState.isOpen
    ) {
      this.handleProps();
    }
  }

  componentWillUnmount() {
    this.removeEvents();
  }

  getContextValue() {
    return {
      toggle: this.toggle,
      isOpen: this.getToggleValue(),
      direction:
        this.props.direction === "down" && this.props.dropup
          ? "up"
          : this.props.direction,
      disabled: this.props.disabled
    };
  }

  getContainer() {
    return this.containerRef.current;
  }

  getToggleValue() {
    return this.props.manual ? this.props.isOpen : this.state.isOpen;
  }

  handleProps() {
    if (this.getToggleValue()) {
      this.addEvents();
    } else {
      this.removeEvents();
    }
  }

  addEvents() {
    if (isFalsy(this.props.manual)) {
      ["click", "touchstart"].forEach(event =>
        document.addEventListener(event, this.handleDocumentClick, true)
      );
    }
  }

  removeEvents() {
    if (isFalsy(this.props.manual)) {
      ["click", "touchstart"].forEach(event =>
        document.removeEventListener(event, this.handleDocumentClick, true)
      );
    }
  }

  handleDocumentClick(e) {
    if (e && e.which === 3) {
      return;
    }

    const container = this.getContainer();

    if (container.contains(e.target) && container !== e.target) {
      return;
    }

    this.toggle(e);
  }

  toggle(e) {
    if (this.props.disabled) {
      return e && e.preventDefault();
    }

    if (!this.props.manual) {
      return this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }

    if (this.props.toggle) {
      return this.props.toggle(e);
    }
  }

  render() {
    const {
      className,
      direction,
      isOpen,
      group,
      size,
      active,
      addonType,
      as,
      ...attrs
    } = omit(["toggle", "disabled", "a11y", "manual"], this.props);

    const Tag = as || (as ? "li" : "div");

    const classes = cx(className, direction !== "down" && `drop${direction}`, {
      [`input-group-${addonType}`]: addonType,
      "btn-group": group,
      [`btn-group-${size}`]: !!size,
      dropdown: !group && !addonType,
      show: this.getToggleValue()
    });

    return (
      <DropdownContext.Provider value={this.getContextValue()}>
        <Manager>
          <Tag
            {...attrs}
            {...{
              [typeof Tag === "string" ? "ref" : "innerRef"]: this.containerRef
            }}
            className={classes}
          />
        </Manager>
      </DropdownContext.Provider>
    );
  }
}
