import Dropdown from "./Dropdown";
import DropdownItem from "./DropdownItem";
import DropdownToggle from "./DropdownToggle";
import DropdownMenu from "./DropdownMenu";

Dropdown.Item = DropdownItem;
Dropdown.Toggle = DropdownToggle;
Dropdown.Menu = DropdownMenu;

export default Dropdown;
