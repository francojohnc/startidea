import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";

const CardImg = React.forwardRef(function(
  { as: Comp, className, isTop, isBottom, ...rest },
  ref
) {
  let prefix = "card-img";

  if (isTop) {
    prefix = "card-img-top";
  } else if (isBottom) {
    prefix = "card-img-bottom";
  }

  return <Comp ref={ref} {...rest} className={cx(className, prefix)} />;
});

CardImg.displayName = "CardImg";

CardImg.propTypes = {
  as: PropTypes.elementType,
  isTop: PropTypes.bool,
  isBottom: PropTypes.bool
};

CardImg.defaultProps = {
  as: "img"
};

export default CardImg;
