import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import CardImg from "./Img";

function generateWithClassName(classNameOuter) {
  return ({ className, ...rest }) => (
    <div {...rest} className={cx(classNameOuter, className)} />
  );
}

const Card = React.forwardRef(function(
  { className, as: Comp, isInverse, isOutline, isBody, variant, ...rest },
  ref
) {
  const prefix = "card";

  const classes = cx(
    className,
    prefix,
    getKeyOnly(isInverse, "text-white"),
    getKeyOnly(isBody, "card-body"),
    getKeyOnly(variant, `${getKeyOnly(isOutline, "border", "bg")}-${variant}`)
  );

  return <Comp ref={ref} {...rest} className={classes} />;
});

Card.displayName = "Card";

Card.propTypes = {
  as: PropTypes.elementType,
  isInverse: PropTypes.bool,
  isOutline: PropTypes.bool,
  isBody: PropTypes.bool,
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
    "info",
    "dark",
    "light"
  ])
};

Card.defaultProps = {
  as: "div"
};

Card.Img = CardImg;

Card.Title = createWithBSPrefix("card-title", {
  Comp: generateWithClassName("h5")
});
Card.Title.displayName = "CardTitle";

Card.Subtitle = createWithBSPrefix("card-subtitle", {
  Comp: generateWithClassName("h6")
});
Card.Subtitle.displayName = "CardSubtitle";

Card.Group = createWithBSPrefix("card-group");
Card.Group.displayName = "CardGroup";

Card.Columns = createWithBSPrefix("card-columns");
Card.Columns.displayName = "CardColumns";

Card.Deck = createWithBSPrefix("card-deck");
Card.Deck.displayName = "CardDeck";

Card.Header = createWithBSPrefix("card-header");
Card.Header.displayName = "CardHeader";

Card.Body = createWithBSPrefix("card-body");
Card.Body.displayName = "CardBody";

Card.Footer = createWithBSPrefix("card-footer");
Card.Footer.displayName = "CardFooter";

Card.ImgOverlay = createWithBSPrefix("card-img-overlay");
Card.ImgOverlay.displayName = "CardImgOverlay";

Card.Link = createWithBSPrefix("card-link", { Comp: "a" });
Card.Link.displayName = "CardLink";

Card.Text = createWithBSPrefix("card-text", { Comp: "span" });
Card.Text.displayName = "CardText";

export default Card;
