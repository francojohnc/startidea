import React, { Fragment } from "react";
import PropTypes from "prop-types";
import SafeAnchor from "components/SafeAnchor";
import Wave from "lib/dom/wave";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import Icon from "components/Icon";
import "./style.min.css";

const propTypes = {
  as: PropTypes.elementType,
  isActive: PropTypes.bool,
  isBlock: PropTypes.bool,
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "info",
    "warning",
    "danger",
    "dark",
    "light",
    "link"
  ]),
  isDisabled: PropTypes.bool,
  isOutline: PropTypes.bool,
  href: PropTypes.string,
  content: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(["small", "large"]),
  icon: PropTypes.string,
  iconType: PropTypes.oneOf(["fontAwesome", "lineAwesome"]),
  iconPosition: PropTypes.oneOf(["left", "right"]),
  shape: PropTypes.oneOf(["rounded", "pill", "circle"]),
  type: PropTypes.oneOf(["button", "reset", "submit"]),
  innerRef: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.func,
    PropTypes.string
  ]),
  children: PropTypes.node,
  className: PropTypes.string
};

const defaultProps = {
  as: "button",
  variant: "primary",
  iconPosition: "left",
  iconType: "lineAwesome",
  isActive: false,
  isDisabled: false,
  isBlock: false,
  type: "button"
};

function Button({
  className,
  isOutline,
  variant,
  isBlock,
  content,
  size,
  shape,
  icon,
  iconType,
  iconPosition,
  isActive,
  isDisabled,
  type,
  children,
  innerRef,
  as: Comp,
  ...props
}) {
  const prefix = "btn";

  const classes = cx(
    className,
    prefix,
    `${prefix}${isOutline ? "-outline" : ""}-${variant}`,
    getKeyOnly(isBlock, `${prefix}-block`),
    getKeyOnly(size, `${prefix}-${size === "small" ? "sm" : "lg"}`),
    getKeyOnly(isActive, "active"),
    getKeyOnly(isDisabled, "disabled"),
    getKeyOnly(shape, `${prefix}--${shape}`),
    getKeyOnly(icon && !(children || content), `${prefix}__icon`),
    getKeyOnly(icon && iconPosition, `${prefix}__icon--${iconPosition}`)
  );

  let iconNode = icon ? <Icon name={icon} type={iconType} /> : undefined;

  const childrenNode = (
    <Fragment>
      {iconPosition === "left" && iconNode}
      {children || content}
      {iconPosition === "right" && iconNode}
    </Fragment>
  );

  if (props.href) {
    return (
      <SafeAnchor
        {...props}
        ref={innerRef}
        className={classes}
        children={childrenNode}
      />
    );
  }

  if (props.to) {
    return (
      <Comp
        {...props}
        ref={innerRef}
        className={classes}
        children={childrenNode}
      />
    );
  }

  const buttonNode = (
    <Comp {...props} type={type} ref={innerRef} className={classes}>
      {childrenNode}
    </Comp>
  );

  return <Wave>{buttonNode}</Wave>;
}

Button.displayName = "Button";
Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
