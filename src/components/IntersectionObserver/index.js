import React, { Component } from "react";
import PropTypes from "prop-types";
import isFunction from "lib/utils/isFunction";
import { unobserve, observe } from "lib/dom/intersection";
import omit from "lib/utils/omit";

function isPlainChildren({ children }) {
  return !isFunction(children);
}

const propTypes = {
  as: PropTypes.elementType,
  onChange: PropTypes.func,
  triggerOnce: PropTypes.bool,
  threshold: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.number)
  ])
};

const defaultProps = {
  as: "div",
  threshold: 0,
  triggerOnce: false
};

export default class IntersectionObserver extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  node = null;

  state = {
    isVisible: false,
    entry: undefined
  };

  constructor(props) {
    super(props);

    this.handleNode = this.handleNode.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (!this.node) {
      console.warn(
        `No DOM node found. Make sure you forward "ref" to the root DOM element you want to observe.`
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { rootMargin, root, threshold, triggerOnce } = this.props;
    const { isVisible } = this.state;

    if (
      prevProps.rootMargin !== rootMargin ||
      prevProps.root !== root ||
      prevProps.threshold !== threshold
    ) {
      unobserve(this.node);
      this.observeNode();
    }

    if (prevState.isVisible !== isVisible && isVisible && triggerOnce) {
      this.resetNode();
    }
  }

  componentWillUnmount() {
    if (this.node) {
      this.resetNode();
    }
  }

  resetNode() {
    unobserve(this.node);
    this.node = null;
  }

  observeNode() {
    if (!this.node) {
      return;
    }

    const { threshold, root, rootMargin } = this.props;

    observe(this.node, this.handleChange, {
      threshold,
      root,
      rootMargin
    });
  }

  handleNode(node = null) {
    if (this.node) {
      unobserve(this.node);
    }

    this.node = node || null;
    this.observeNode();
  }

  handleChange(isVisible, entry) {
    // Only trigger a state update if isVisible has changed.
    // This prevents an unnecessary extra state update during mount,
    // when the element stats outside the viewport
    if (isVisible !== this.state.isVisible || isVisible) {
      this.setState({
        isVisible,
        entry
      });
    }

    if (this.props.onChange) {
      // If the user is actively listening for onChange, always trigger it
      this.props.onChange({ isVisible, entry });
    }
  }

  render() {
    const { isVisible, entry } = this.state;

    if (!isPlainChildren(this.props)) {
      return this.props.children({
        isVisible,
        entry,
        ref: this.handleNode
      });
    }

    const { as: Comp, ...rest } = omit(
      ["onChange", "threshold", "root", "rootMargin", "triggerOnce"],
      this.props
    );

    return <Comp ref={this.handleNode} {...rest} />;
  }
}
