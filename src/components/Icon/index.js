import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cx from "lib/dom/classnames";
import isArray from "lib/utils/isArray";
import throwError from "lib/utils/throwError";
import omit from "lib/utils/omit";

const TypeProp = PropTypes.oneOf(["fontAwesome", "lineAwesome"]);

const IconProp = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.arrayOf(PropTypes.string)
]);

const FlipProp = PropTypes.oneOf(["horizontal", "vertical", "both"]);
const SizeProp = PropTypes.oneOf([
  "xs",
  "lg",
  "sm",
  "1x",
  "2x",
  "3x",
  "4x",
  "5x",
  "6x",
  "7x",
  "8x",
  "9x",
  "10x"
]);

const PullProp = PropTypes.oneOf(["left", "right"]);
const RotateProp = PropTypes.oneOf([90, 180, 270]);
const TransformProp = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.shape({
    size: PropTypes.number,
    x: PropTypes.number,
    y: PropTypes.number,
    rotate: PropTypes.number,
    flipX: PropTypes.bool,
    flipY: PropTypes.bool
  })
]);

const FaSymbolProp = PropTypes.oneOfType([PropTypes.string, PropTypes.bool]);

Icon.propTypes = {
  type: TypeProp,
  name: IconProp,
  mask: IconProp,
  color: PropTypes.string,
  spin: PropTypes.bool,
  pulse: PropTypes.bool,
  border: PropTypes.bool,
  fixedWidth: PropTypes.bool,
  inverse: PropTypes.bool,
  listItem: PropTypes.bool,
  flip: FlipProp,
  size: SizeProp,
  pull: PullProp,
  rotation: RotateProp,
  transform: TransformProp,
  symbol: FaSymbolProp
};

Icon.defaultProps = {
  type: "lineAwesome"
};

export default function Icon({ className, name, type, ...rest }) {
  let finalProps = rest;
  let classes = className;

  if (type === "lineAwesome" && isArray(name)) {
    throwError("Use string icon when using lineAwesome");
  }

  if (type === "lineAwesome") {
    finalProps = omit(
      [
        "mask",
        "color",
        "spin",
        "pulse",
        "border",
        "fixedWidth",
        "inverse",
        "listItem",
        "flip",
        "size",
        "pull",
        "rotation",
        "transform",
        "symbol"
      ],
      rest
    );

    classes = cx(className, `la la-${name}`);

    return <i {...finalProps} className={classes} />;
  }

  return <FontAwesomeIcon {...finalProps} className={classes} icon={name} />;
}
