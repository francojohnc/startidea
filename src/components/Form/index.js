import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import "./style.min.css";

const propTypes = {
  isInline: PropTypes.bool,
  isLoading: PropTypes.bool
};

const defaultProps = {
  isInline: false,
  isLoading: false
};

function Form({ className, isInline, isLoading, ...props }) {
  const prefix = "form";

  return (
    <form
      {...props}
      className={cx(
        className,
        getKeyOnly(isLoading, `${prefix}--loading`),
        getKeyOnly(isInline, `${prefix}-inline`)
      )}
    />
  );
}

Form.displayName = "Form";
Form.Group = createWithBSPrefix("form-group");
Form.Group.displayName = "FormGroup";

Form.Row = createWithBSPrefix("form-row");
Form.Row.displayName = "FormRow";

Form.Label = createWithBSPrefix("form-label", { Comp: "label" });
Form.Label.displayName = "FormLabel";

Form.propTypes = propTypes;
Form.defaultProps = defaultProps;

export default Form;
