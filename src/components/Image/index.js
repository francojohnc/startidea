import React, { Component } from "react";
import PropTypes from "prop-types";
import NotFoundImg from "./not-found.png";

const notFoundStyle = {
  maxHeight: "80px"
};

const propTypes = {
  as: PropTypes.elementType,
  src: PropTypes.string.isRequired,
  loader: PropTypes.element,
  unloader: PropTypes.element
};

const defaultProps = {
  as: "img"
};

export default class Image extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.state = { isLoading: false, isLoaded: false, hasError: false };

    this.loadImg = this.loadImg.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  componentDidMount() {
    this.initImg();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.src !== this.props.src) {
      this.initImg();
    }
  }

  componentWillUnmount() {
    if (this.img) {
      this.unloadImg();
    }
  }

  initImg() {
    this.setState({ isLoading: true }, this.loadImg);
  }

  loadImg() {
    this.img = new window.Image();
    this.img.src = this.props.src;
    this.img.onload = this.handleLoad;
    this.img.onerror = this.handleError;
  }

  unloadImg() {
    this.img.onerror = null;
    this.img.onload = null;

    this.img.src = "";
    this.img = null;
  }

  handleLoad() {
    if (this.img) {
      this.setState({ isLoaded: true, isLoading: false });
    }
  }

  handleError(e) {
    if (!this.img || !this.props.src) {
      return false;
    }

    this.setState({ isLoaded: true, isLoading: false, hasError: true });
  }

  render() {
    const { as: Comp, loader, unloader, src, ...rest } = this.props;
    const { isLoaded, isLoading, hasError } = this.state;

    if (isLoaded && !hasError) {
      const imgProps = {
        ...{ src },
        ...rest
      };
      // eslint-disable-next-line jsx-a11y/alt-text
      return <Comp {...imgProps} />;
    }

    // if we are still trying to load, show img and a loader if requested
    if (!isLoaded && isLoading) {
      return loader || null;
    }

    if ((!isLoaded && !isLoading) || hasError) {
      return (
        unloader || (
          <img
            src={NotFoundImg}
            alt="Something went wrong..."
            style={notFoundStyle}
          />
        )
      );
    }
  }
}
