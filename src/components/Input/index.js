import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import Icon from "components/Icon";
import {getPrefix, hasIcon} from "./utils";
import "./style.min.css";

const propTypes = {
    as: PropTypes.elementType,
    icon: PropTypes.string,
    iconType: PropTypes.oneOf(["fontAwesome", "lineAwesome"]),
    iconVariant: PropTypes.oneOf([
        "primary",
        "secondary",
        "success",
        "info",
        "warning",
        "danger",
        "dark",
        "light",
        "link",
        "body",
        "muted",
        "white",
        "black-50",
        "white-50"
    ]),
    iconPosition: PropTypes.oneOf(["left", "right"]),
    size: PropTypes.oneOf(["small", "large"]),
    shape: PropTypes.oneOf(["rounded", "pill"]),
    isPlaintext: PropTypes.bool,
    isReadOnly: PropTypes.bool,
    type: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    isValid: PropTypes.bool,
    isInvalid: PropTypes.bool,
    isCustom: PropTypes.bool,
    isStatic: PropTypes.bool
};

const defaultProps = {
    as: "input",
    type: "text",
    iconType: "lineAwesome",
    iconPosition: "left",
    isPlaintext: false,
    isReadOnly: false,
    isValid: false,
    isInvalid: false,
    isCustom: false,
    isStatic: false
};

const InputIcon = ({icon, position, variant, type, isValid, isInvalid}) => {
    const classes = cx("input-icon__icon", `input-icon__icon--${position}`);
    const iconClasses = cx(
        getKeyOnly(
            variant || isValid || isInvalid,
            `text-${isValid ? "success" : isInvalid ? "danger" : variant}`
        ),
        getKeyOnly(type === "lineAwesome", `la la-${icon}`)
    );

    return (
        <span className={classes}>
      <span>
        <Icon type={type} name={icon} className={iconClasses}/>
      </span>
    </span>
    );
};

const Input = React.forwardRef(
    (
        {
            type,
            size,
            icon,
            shape,
            isValid,
            isCustom,
            isStatic,
            iconType,
            className,
            isInvalid,
            isReadOnly,
            isPlaintext,
            iconVariant,
            iconPosition,
            inputClassName,
            as: Comp,
            ...props
        },
        ref
    ) => {
        let prefix = getPrefix({isPlaintext, isCustom, type});
        const _hasIcon = hasIcon(icon);

        const classes = cx(
            prefix,
            _hasIcon ? inputClassName : className,
            getKeyOnly(size, `${prefix}-${size === "small" ? "sm" : "lg"}`),
            getKeyOnly(isStatic, "position-static"),
            getKeyOnly(isValid, "is-valid"),
            getKeyOnly(isInvalid, "is-invalid"),
            getKeyOnly(shape, `input--${shape}`)
        );

        const InnerComp = (
            <Comp {...props} type={type} ref={ref} className={classes}/>
        );

        if (!_hasIcon) {
            return InnerComp;
        }

        const Icon = (
            <InputIcon
                icon={icon}
                position={iconPosition}
                variant={iconVariant}
                type={iconType}
                isValid={isValid}
                isInvalid={isInvalid}
            />
        );

        const wrapperClasses = cx(
            "input-icon",
            `input-icon--${iconPosition}`,
            className
        );

        return (
            <div className={wrapperClasses}>
                {InnerComp}
                {Icon}
            </div>
        );
    }
);

Input.displayName = "Input";
Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

export default Input;
