import getKeyOnly from "lib/dom/getKeyOnly";
import isCheckableInput from "lib/dom/isCheckableInput";
import isSwitchInput from "lib/dom/isSwitchInput";
import isRangeInput from "lib/dom/isRangeInput";
import isFileInput from "lib/utils/isFileInput";
import throwError from "lib/utils/throwError";
import isSelectInput from "lib/utils/isSelectInput";
import isSomeTrue from "lib/utils/isSomeTrue";
import isNil from "lib/utils/isNil";

export const getPrefix = ({ isPlaintext, isCustom, type }) => {
  const prefix = "form-control";

  if (!isCustom && isSwitchInput(type)) {
    throwError("`isCustom` cannot be set to `false` when the type is `switch`");
  }

  if (isCustom) {
    if (isCheckableInput(type)) {
      return "custom-control-input";
    }

    if (isSomeTrue(isSelectInput(type), isRangeInput(type))) {
      return `custom-${type}`;
    }

    if (isFileInput(type)) {
      return "custom-file-input";
    }

    throwError(
      "`isCustom` cannot be set to `true` when the type is not checkable or selectable"
    );
  }

  if (isPlaintext) {
    return getKeyOnly(isPlaintext, `${prefix}-plaintext`);
  } else if (isFileInput(type)) {
    return `${prefix}-file`;
  } else if (isCheckableInput(type)) {
    return "form-check-input";
  }

  return prefix;
};

export const hasIcon = icon => {
  return !isNil(icon);
};
