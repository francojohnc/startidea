import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import ListGroupItem from "./item";
import ListGroupItemContent from "./content";

ListGroup.propTypes = {
  as: PropTypes.elementType,
  isFlush: PropTypes.bool,
  relaxed: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["very"])])
};

ListGroup.defaultProps = {
  as: "ul",
  isFlush: false
};

function ListGroup({ className, as: Comp, isFlush, relaxed, ...rest }) {
  const classes = cx(
    className,
    "list-group",
    getKeyOnly(isFlush, "list-group-flush"),
    getKeyOnly(relaxed === "very", "very"),
    getKeyOnly(relaxed, "relaxed")
  );

  return <Comp {...rest} className={classes} />;
}

ListGroup.Item = ListGroupItem;
ListGroup.Content = ListGroupItemContent;
ListGroup.Header = createWithBSPrefix("list-group-item-heading", {
  Comp: "h5"
});
ListGroup.Description = createWithBSPrefix("list-group-item-text", {
  Comp: "p"
});

export default ListGroup;
