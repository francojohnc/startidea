import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";

ListGroupItemContent.propTypes = {
  as: PropTypes.elementType,
  content: PropTypes.node,
  floated: PropTypes.oneOf(["left", "right"])
};

ListGroupItemContent.defaultProps = {
  as: "div",
  content: ""
};

function ListGroupItemContent({
  className,
  as: Comp,
  content,
  children,
  floated,
  ...rest
}) {
  const prefix = "list-group-item-content";
  const classes = cx(
    className,
    prefix,
    getKeyOnly(floated, `float-${floated}`)
  );

  return <Comp {...rest} className={classes} children={children || content} />;
}

export default ListGroupItemContent;
