import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";

ListGroupItem.propTypes = {
  as: PropTypes.elementType,
  isActive: PropTypes.bool,
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "info",
    "warning",
    "danger",
    "dark",
    "light"
  ]),
  isAction: PropTypes.bool,
  isDisabled: PropTypes.bool
};

ListGroupItem.defaultProps = {
  as: "li"
};

const handleDisabledOnClick = e => {
  e.preventDefault();
};

function ListGroupItem({
  className,
  as: Comp,
  isActive,
  isDisabled,
  isAction,
  variant,
  ...rest
}) {
  const prefix = "list-group-item";
  const classes = cx(
    className,
    prefix,
    getKeyOnly(isActive, "active"),
    getKeyOnly(isDisabled, "disabled"),
    getKeyOnly(isAction, "list-group-item-action"),
    getKeyOnly(variant, `list-group-item-${variant}`)
  );

  // Prevent click event when disabled.
  if (isDisabled) {
    rest.onClick = handleDisabledOnClick;
  }

  return <Comp role="listitem" {...rest} className={classes} />;
}

export default ListGroupItem;
