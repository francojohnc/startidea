import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";

ListGroupItemContent.propTypes = {
  as: PropTypes.elementType
};

ListGroupItemContent.defaultProps = {
  as: "div"
};

function ListGroupItemContent({ className, as: Comp, ...rest }) {
  const prefix = "list-group-item-content";
  const classes = cx(className, prefix);

  return <Comp {...rest} className={classes} />;
}

export default ListGroupItemContent;
