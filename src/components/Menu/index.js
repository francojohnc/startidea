import React, { Component } from "react";
import PropTypes from "prop-types";
import { SIZES } from "components/global/constants";
import getElementType from "lib/dom/getElementType";
import cx from "lib/dom/classnames";
import omit from "lib/utils/omit";
import getKeyOnly from "lib/dom/getKeyOnly";
import getKeyOrValueAndKey from "lib/dom/getKeyOrValueAndKey";
import { SidebarContext } from "../Layout/Sidebar";
import { MenuProvider } from "./MenuContext";
import MenuItem from "./MenuItem";
import "./style.min.css";

const propTypes = {
  isBorderless: PropTypes.bool,
  isFluid: PropTypes.bool,
  isStackable: PropTypes.bool,
  isVertical: PropTypes.bool,
  isDark: PropTypes.bool,
  as: PropTypes.elementType,
  activeIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  defaultActiveIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  icon: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["labeled"])]),
  iconType: PropTypes.oneOf(["fontAwesome", "lineAwesome"]),
  size: PropTypes.oneOf(omit(["medium", "big"], SIZES))
};

const defaultProps = {
  iconType: "lineAwesome"
};

export default class Menu extends Component {
  static contextType = SidebarContext;
  static propTypes = propTypes;
  static defaultProps = defaultProps;
  static Item = MenuItem;

  render() {
    const {
      isBorderless,
      className,
      isFluid,
      icon,
      size,
      isStackable,
      isVertical,
      isDark,
      iconType,
      ...rest
    } = this.props;
    const { isSidebarCollapsed } = this.context;

    const classes = cx(
      className,
      "menu",
      getKeyOnly(size, `menu--${size}`),
      getKeyOnly(isDark, "menu--dark"),
      getKeyOnly(isBorderless, "menu--borderless"),
      getKeyOnly(isFluid, "menu--fluid"),
      getKeyOnly(isStackable, "menu--stackable"),
      getKeyOnly(isVertical, "menu--vertical"),
      getKeyOnly(isSidebarCollapsed, "menu--collapsed"),
      getKeyOrValueAndKey(icon, "menu--icon")
    );

    const Comp = getElementType(Menu, this.props);

    return (
      <MenuProvider value={{ iconType }}>
        <Comp {...rest} className={classes} />
      </MenuProvider>
    );
  }
}
