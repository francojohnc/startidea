import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import getElementType from "lib/dom/getElementType";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import MenuContext from "./MenuContext";
import isEmpty from "lib/utils/isEmpty";
import Icon from "components/Icon";

const propTypes = {
  as: PropTypes.elementType,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  header: PropTypes.bool,
  icon: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  content: PropTypes.node,
  index: PropTypes.number,
  link: PropTypes.bool,
  name: PropTypes.string,
  onClick: PropTypes.func,
  position: PropTypes.oneOf(["left", "right"])
};

const defaultProps = {
  onClick: () => null
};

export default class MenuItem extends Component {
  static contextType = MenuContext;
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    const { disabled } = this.props;

    if (!disabled) {
      this.props.onClick(e, this.props);
    }
  }

  render() {
    const {
      active,
      children,
      className,
      content,
      disabled,
      header,
      icon,
      link,
      name,
      onClick,
      position,
      ...rest
    } = this.props;
    const { iconType } = this.context;

    const classes = cx(
      className,
      "menu__item",
      position,
      getKeyOnly(active, "menu__item--active"),
      getKeyOnly(disabled, "menu__item--disabled"),
      getKeyOnly(icon === true || (icon && !name), "menu__item--icon"),
      getKeyOnly(header, "menu__item--header"),
      getKeyOnly(link, "menu__item--link")
    );

    const ElementType = getElementType(MenuItem, this.props, () => {
      if (onClick) return "a";
    });

    return (
      <ElementType {...rest} className={classes} onClick={this.handleClick}>
        {!isEmpty(children) ? (
          children
        ) : (
          <Fragment>
            <Icon name={icon} type={iconType} />
            <span title={content}>{content}</span>
          </Fragment>
        )}
      </ElementType>
    );
  }
}
