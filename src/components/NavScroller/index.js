import React from "react";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import cx from "lib/dom/classnames";
import "./styles.min.css";

function NavScroller({ className, children, ...rest }) {
  const classes = cx(className, "nav-scroller-nav");

  return (
    <nav className={classes} {...rest}>
      <div className="nav-scroller-content">{children}</div>
    </nav>
  );
}

NavScroller.Item = createWithBSPrefix("nav-scroller-item");

export default NavScroller;
