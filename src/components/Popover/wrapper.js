import React, { Component } from "react";
import PropTypes from "prop-types";
import { POPPER_PLACEMENTS } from "./constants";
import getTarget from "lib/dom/getTarget";
import omit from "lib/utils/omit";
import PopperContent from "./content";

export const targetPropType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.func,
  PropTypes.element,
  PropTypes.shape({ current: PropTypes.any })
]);

export const propTypes = {
  placement: PropTypes.oneOf(POPPER_PLACEMENTS),
  target: targetPropType.isRequired,
  container: targetPropType,
  isOpen: PropTypes.bool,
  disabled: PropTypes.bool,
  hideArrow: PropTypes.bool,
  boundariesElement: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  className: PropTypes.string,
  innerClassName: PropTypes.string,
  arrowClassName: PropTypes.string,
  popperClassName: PropTypes.string,
  toggle: PropTypes.func,
  autohide: PropTypes.bool,
  placementPrefix: PropTypes.string,
  modifiers: PropTypes.object,
  offset: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  innerRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
    PropTypes.object
  ]),
  trigger: PropTypes.string,
  fade: PropTypes.bool,
  flip: PropTypes.bool
};

const propKeys = Object.keys(propTypes);

const DEFAULT_DELAYS = {
  show: 0,
  hide: 0
};

const defaultProps = {
  isOpen: false,
  hideArrow: false,
  autohide: false,
  toggle: function() {},
  trigger: "click",
  fade: true
};

function isInDOMSubtree(element, subtreeRoot) {
  return (
    subtreeRoot && (element === subtreeRoot || subtreeRoot.contains(element))
  );
}

export default class PopoverWrapper extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this._target = null;
    this.addTargetEvents = this.addTargetEvents.bind(this);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.removeTargetEvents = this.removeTargetEvents.bind(this);
    this.toggle = this.toggle.bind(this);
    this.showWithDelay = this.showWithDelay.bind(this);
    this.hideWithDelay = this.hideWithDelay.bind(this);
    this.onMouseOverTooltipContent = this.onMouseOverTooltipContent.bind(this);
    this.onMouseLeaveTooltipContent = this.onMouseLeaveTooltipContent.bind(
      this
    );
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.onEscKeyDown = this.onEscKeyDown.bind(this);
    this.getRef = this.getRef.bind(this);
    this.onClosed = this.onClosed.bind(this);
    this.state = { isOpen: props.isOpen };
    this._isMounted = false;
  }

  static getDerivedStateFromProps(props, state) {
    return props.isOpen && !state.isOpen ? { isOpen: props.isOpen } : null;
  }

  componentDidMount() {
    this._isMounted = true;
    this.updateTarget();
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.removeTargetEvents();
  }

  updateTarget() {
    const newTarget = getTarget(this.props.target);
    if (newTarget !== this._target) {
      this.removeTargetEvents();
      this._target = newTarget;
      this.addTargetEvents();
    }
  }

  addTargetEvents() {
    if (this.props.trigger) {
      const triggers = this.props.trigger.split(" ");

      if (triggers.indexOf("manual") === -1) {
        if (triggers.indexOf("click") > -1 || triggers.indexOf("legacy") > -1) {
          document.addEventListener("click", this.handleDocumentClick, true);
        }

        if (this._target) {
          if (triggers.indexOf("hover") > -1) {
            this._target.addEventListener(
              "mouseover",
              this.showWithDelay,
              true
            );
            this._target.addEventListener("mouseout", this.hideWithDelay, true);
          }
          if (triggers.indexOf("focus") > -1) {
            this._target.addEventListener("focusin", this.show, true);
            this._target.addEventListener("focusout", this.hide, true);
          }

          this._target.addEventListener("keydown", this.onEscKeyDown, true);
        }
      }
    }
  }

  removeTargetEvents() {
    if (this._target) {
      this._target.removeEventListener("mouseover", this.showWithDelay, true);
      this._target.removeEventListener("mouseout", this.hideWithDelay, true);
      this._target.removeEventListener("keydown", this.onEscKeyDown, true);
      this._target.removeEventListener("focusin", this.show, true);
      this._target.removeEventListener("focusout", this.hide, true);
    }

    document.removeEventListener("click", this.handleDocumentClick, true);
  }

  toggle(e) {
    if (this.props.disabled || !this._isMounted) {
      return e && e.preventDefault();
    }

    return this.props.toggle(e);
  }

  onClosed() {
    this.setState({ isOpen: false });
  }

  onMouseOverTooltipContent() {
    if (this.props.trigger.indexOf("hover") > -1 && !this.props.autohide) {
      if (this._hideTimeout) {
        this.clearHideTimeout();
      }
      if (this.state.isOpen && !this.props.isOpen) {
        this.toggle();
      }
    }
  }

  onMouseLeaveTooltipContent(e) {
    if (this.props.trigger.indexOf("hover") > -1 && !this.props.autohide) {
      if (this._showTimeout) {
        this.clearShowTimeout();
      }
      e.persist();
      this._hideTimeout = setTimeout(
        this.hide.bind(this, e),
        this.getDelay("hide")
      );
    }
  }

  onEscKeyDown(e) {
    if (e.key === "Escape") {
      this.hide(e);
    }
  }

  getRef(ref) {
    const { innerRef } = this.props;
    if (innerRef) {
      if (typeof innerRef === "function") {
        innerRef(ref);
      } else if (typeof innerRef === "object") {
        innerRef.current = ref;
      }
    }
    this._popover = ref;
  }

  getDelay(key) {
    const { delay } = this.props;
    if (typeof delay === "object") {
      return isNaN(delay[key]) ? DEFAULT_DELAYS[key] : delay[key];
    }
    return delay;
  }

  show(e) {
    if (!this.props.isOpen) {
      this.clearShowTimeout();
      this.toggle(e);
    }
  }

  showWithDelay(e) {
    if (this._hideTimeout) {
      this.clearHideTimeout();
    }
    this._showTimeout = setTimeout(
      this.show.bind(this, e),
      this.getDelay("show")
    );
  }
  hide(e) {
    if (this.props.isOpen) {
      this.clearHideTimeout();
      this.toggle(e);
    }
  }

  hideWithDelay(e) {
    if (this._showTimeout) {
      this.clearShowTimeout();
    }
    this._hideTimeout = setTimeout(
      this.hide.bind(this, e),
      this.getDelay("hide")
    );
  }

  clearShowTimeout() {
    clearTimeout(this._showTimeout);
    this._showTimeout = undefined;
  }

  clearHideTimeout() {
    clearTimeout(this._hideTimeout);
    this._hideTimeout = undefined;
  }

  handleDocumentClick(e) {
    const triggers = this.props.trigger.split(" ");

    if (
      triggers.indexOf("legacy") > -1 &&
      (this.props.isOpen || isInDOMSubtree(e.target, this._target))
    ) {
      if (this._hideTimeout) {
        this.clearHideTimeout();
      }
      if (this.props.isOpen && !isInDOMSubtree(e.target, this._popover)) {
        this.hideWithDelay(e);
      } else if (!this.props.isOpen) {
        this.showWithDelay(e);
      }
    } else if (
      triggers.indexOf("click") > -1 &&
      isInDOMSubtree(e.target, this._target)
    ) {
      if (this._hideTimeout) {
        this.clearHideTimeout();
      }

      if (!this.props.isOpen) {
        this.showWithDelay(e);
      } else {
        this.hideWithDelay(e);
      }
    }
  }

  render() {
    if (!this.state.isOpen) {
      return null;
    }

    this.updateTarget();

    const {
      className,
      innerClassName,
      target,
      isOpen,
      hideArrow,
      boundariesElement,
      placement,
      placementPrefix,
      arrowClassName,
      popperClassName,
      container,
      modifiers,
      offset,
      fade,
      flip
    } = this.props;

    const unhandledProps = omit(propKeys, this.props);

    return (
      <PopperContent
        className={className}
        target={target}
        isOpen={isOpen}
        hideArrow={hideArrow}
        boundariesElement={boundariesElement}
        placement={placement}
        placementPrefix={placementPrefix}
        arrowClassName={arrowClassName}
        popperClassName={popperClassName}
        container={container}
        modifiers={modifiers}
        offset={offset}
        onClosed={this.onClosed}
        fade={fade}
        flip={flip}
      >
        <div
          {...unhandledProps}
          ref={this.getRef}
          className={innerClassName}
          role="tooltip"
          aria-hidden={isOpen}
          onMouseOver={this.onMouseOverTooltipContent}
          onMouseLeave={this.onMouseLeaveTooltipContent}
          onKeyDown={this.onEscKeyDown}
        />
      </PopperContent>
    );
  }
}
