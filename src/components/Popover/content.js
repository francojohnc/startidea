import React, { Component } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";
import { Popper as ReactPopper } from "react-popper";
import getTarget from "lib/dom/getTarget";
import cx from "lib/dom/classnames";
import Fade, { FadeDefaultProps, FadePropTypes } from "../Fade";

const targetPropType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.func,
  PropTypes.element,
  PropTypes.shape({ current: PropTypes.any })
]);

function noop() {}

const propTypes = {
  as: PropTypes.elementType,
  popperClassName: PropTypes.string,
  placement: PropTypes.string,
  placementPrefix: PropTypes.string,
  arrowClassName: PropTypes.string,
  hideArrow: PropTypes.bool,
  isOpen: PropTypes.bool.isRequired,
  offset: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  fallbackPlacement: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  flip: PropTypes.bool,
  container: targetPropType,
  target: targetPropType.isRequired,
  modifiers: PropTypes.object,
  boundariesElement: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  onClosed: PropTypes.func,
  fade: PropTypes.bool,
  transition: PropTypes.shape(FadePropTypes)
};

const defaultProps = {
  boundariesElement: "scrollParent",
  placement: "auto",
  hideArrow: false,
  isOpen: false,
  offset: 0,
  fallbackPlacement: "flip",
  flip: true,
  container: "body",
  modifiers: {},
  onClosed: noop,
  fade: true,
  transition: { ...FadeDefaultProps }
};

export default class PopperContent extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.handlePlacementChange = this.handlePlacementChange.bind(this);
    this.setTargetNode = this.setTargetNode.bind(this);
    this.getTargetNode = this.getTargetNode.bind(this);
    this.getRef = this.getRef.bind(this);
    this.onClosed = this.onClosed.bind(this);
    this.state = { isOpen: props.isOpen };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.isOpen && !state.isOpen) {
      return { isOpen: props.isOpen };
    } else return null;
  }

  componentDidUpdate() {
    if (
      this._element &&
      this._element.childNodes &&
      this._element.childNodes[0] &&
      this._element.childNodes[0].focus
    ) {
      this._element.childNodes[0].focus();
    }
  }

  setTargetNode(node) {
    this.targetNode = node;
  }

  getTargetNode() {
    return this.targetNode;
  }

  getContainerNode() {
    return getTarget(this.props.container);
  }

  getRef(ref) {
    this._element = ref;
  }

  handlePlacementChange(data) {
    if (this.state.placement !== data.placement) {
      this.setState({ placement: data.placement });
    }
    return data;
  }

  onClosed() {
    this.props.onClosed();
    this.setState({ isOpen: false });
  }

  renderChildren() {
    const {
      children,
      isOpen,
      flip,
      target,
      offset,
      fallbackPlacement,
      placementPrefix,
      arrowClassName: _arrowClassName,
      hideArrow,
      popperClassName: _popperClassName,
      as,
      container,
      modifiers,
      boundariesElement,
      onClosed,
      fade,
      transition,
      ...attrs
    } = this.props;
    const arrowClassName = cx("arrow", _arrowClassName);
    const placement = this.state.placement || attrs.placement;
    const placementFirstPart = placement.split("-")[0];
    const popperClassName = cx(
      _popperClassName,
      placementPrefix
        ? `${placementPrefix}-${placementFirstPart}`
        : placementFirstPart
    );

    const extendedModifiers = {
      offset: { offset },
      flip: { enabled: flip, behavior: fallbackPlacement },
      preventOverflow: { boundariesElement },
      update: {
        enabled: true,
        order: 950,
        fn: this.handlePlacementChange
      },
      ...modifiers
    };

    const popperTransition = {
      ...FadeDefaultProps,
      ...transition,
      baseClass: fade ? transition.baseClass : "",
      timeout: fade ? transition.timeout : 0
    };

    return (
      <Fade
        {...popperTransition}
        {...attrs}
        as={as}
        in={isOpen}
        onExited={this.onClosed}
      >
        <React.Fragment>
          {isOpen && (
            <ReactPopper
              referenceElement={this.targetNode}
              modifiers={extendedModifiers}
              placement={placement}
            >
              {({ ref, style, placement, arrowProps }) => (
                <div
                  ref={ref}
                  style={style}
                  className={popperClassName}
                  x-placement={placement}
                >
                  {children}
                  {!hideArrow && (
                    <span
                      ref={arrowProps.ref}
                      className={arrowClassName}
                      style={arrowProps.style}
                    />
                  )}
                </div>
              )}
            </ReactPopper>
          )}
        </React.Fragment>
      </Fade>
    );
  }

  render() {
    this.setTargetNode(getTarget(this.props.target));

    if (this.state.isOpen) {
      return this.props.container === "inline"
        ? this.renderChildren()
        : createPortal(
            <div ref={this.getRef}>{this.renderChildren()}</div>,
            this.getContainerNode()
          );
    }

    return null;
  }
}
