import React from "react";
import cx from "lib/dom/classnames";
import TooltipPopoverWrapper, { propTypes } from "./wrapper";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";

const defaultProps = {
  placement: "right",
  placementPrefix: "bs-popover",
  trigger: "click"
};

const Popover = ({ innerClassName, ...rest }) => {
  const popperClasses = cx("popover", "show");
  const classes = cx("popover-inner", innerClassName);

  return (
    <TooltipPopoverWrapper
      {...rest}
      popperClassName={popperClasses}
      innerClassName={classes}
    />
  );
};

Popover.propTypes = propTypes;
Popover.defaultProps = defaultProps;
Popover.Header = createWithBSPrefix("popover-header", { Comp: "h3" });
Popover.Body = createWithBSPrefix("popover-body");

export default Popover;
