export const numberToWordMap = {
  1: "one",
  2: "two",
  3: "three",
  4: "four",
  5: "five",
  6: "six",
  7: "seven",
  8: "eight",
  9: "nine",
  10: "ten",
  11: "eleven",
  12: "twelve"
};

/**
 * Return the number word for numbers 1-12.
 * Returns strings or numbers as is if there is no corresponding word.
 * Returns an empty string if value is not a string or number.
 * @param {string|number} value The value to convert to a word.
 * @returns {string}
 */
export default function numberToWord(value) {
  const type = typeof value;

  if (type === "string" || type === "number") {
    return numberToWordMap[value] || value;
  }

  return "";
}
