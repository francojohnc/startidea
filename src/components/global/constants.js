import { numberToWordMap } from "./numberToWord";

export const WIDTHS = [
  ...Object.keys(numberToWordMap),
  ...Object.keys(numberToWordMap).map(Number),
  ...Object.values(numberToWordMap)
];

export const SIZES = [
  "mini",
  "tiny",
  "small",
  "medium",
  "large",
  "big",
  "huge",
  "massive"
];
