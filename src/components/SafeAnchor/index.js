import React from "react";
import PropTypes from "prop-types";
import createChainedFunction from "lib/utils/createChainedFunction";

const propTypes = {
  href: PropTypes.string,
  to: PropTypes.string,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
  disabled: PropTypes.bool,
  role: PropTypes.string,
  tabIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  as: PropTypes.elementType
};

const defaultProps = {
  as: "a"
};

function isTrivialHref(href) {
  return !href || href.trim() === "#";
}

function getLink(href, to) {
  if (href) {
    return ["href", href];
  }

  return ["to", to];
}

function SafeAnchor({ as: Comp, disabled, onKeyDown, ...props }) {
  const { href, to, onClick } = props;
  const [hrefProp, link] = getLink(href, to);
  const isTrivialLink = isTrivialHref(link);

  const handleClick = e => {
    if (disabled || isTrivialLink) {
      e.preventDefault();
    }

    if (disabled) {
      e.stopPropagation();
      return;
    }

    if (onClick) {
      onClick(e);
    }
  };

  const handleKeyDown = event => {
    if (event.key === " ") {
      event.preventDefault();
      handleClick(event);
    }
  };

  if (isTrivialLink) {
    props.role = props.role || "button";
    props[hrefProp] = link || "#";
  }

  if (disabled) {
    props.tabIndex = -1;
    props["aria-disabled"] = true;
  }

  return (
    <Comp
      {...props}
      onClick={handleClick}
      onKeyDown={createChainedFunction(handleKeyDown, onKeyDown)}
    />
  );
}

SafeAnchor.displayName = "SafeAnchor";
SafeAnchor.propTypes = propTypes;
SafeAnchor.defaultProps = defaultProps;

export default SafeAnchor;
