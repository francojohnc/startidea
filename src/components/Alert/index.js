import React, { useState } from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import Fade, { FadeDefaultProps } from "components/Fade";
import getKeyOnly from "lib/dom/getKeyOnly";
import Icon from "components/Icon";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";

const propTypes = {
  as: PropTypes.elementType,
  className: PropTypes.string,
  closeClassName: PropTypes.string,
  closeAriaLabel: PropTypes.string,
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "info",
    "warning",
    "danger",
    "dark",
    "light"
  ]),
  isFade: PropTypes.bool,
  isOpen: PropTypes.bool,
  isAuto: PropTypes.bool,
  toggle: PropTypes.func
};

const defaultProps = {
  as: "div",
  variant: "primary",
  closeAriaLabel: "Close",
  isOpen: true,
  isFade: true,
  isAuto: false
};

const FadeTransition = {
  ...FadeDefaultProps,
  unmountOnExit: true
};

function Alert(props) {
  const {
    className,
    closeClassName,
    closeAriaLabel,
    as: Comp,
    variant,
    isOpen,
    toggle,
    children,
    isFade,
    isAuto,
    icon,
    ...rest
  } = props;

  const [isManualOpen, setManualOpen] = useState(true);

  const toggleManually = () => setManualOpen(!isManualOpen);

  const classes = cx(
    className,
    "alert",
    getKeyOnly(variant, `alert-${variant}`),
    getKeyOnly(toggle, "alert-dismissible")
  );

  const closeClasses = cx("close", closeClassName);

  const alertTransition = {
    ...FadeDefaultProps,
    ...FadeTransition,
    baseClass: isFade ? FadeTransition.baseClass : "",
    timeout: isFade ? FadeTransition.timeout : 0
  };

  return (
    <Fade
      {...rest}
      {...alertTransition}
      as={Comp}
      className={classes}
      in={isAuto ? isManualOpen : isOpen}
      role="alert"
    >
      {!icon ? <div className="alert-text">{children}</div> : null}
      {toggle || isAuto ? (
        <div className="alert-close">
          <button
            type="button"
            tabIndex="-1"
            className={closeClasses}
            aria-label={closeAriaLabel}
            onClick={toggle || toggleManually}
          >
            <span aria-hidden="true">
              <Icon name="close" />
            </span>
          </button>
        </div>
      ) : null}
    </Fade>
  );
}

Alert.Header = createWithBSPrefix("alert-heading", { Comp: "h4" });
Alert.Header.displayName = "AlertHeader";

Alert.displayName = "Alert";
Alert.propTypes = propTypes;
Alert.defaultProps = defaultProps;

export default Alert;
