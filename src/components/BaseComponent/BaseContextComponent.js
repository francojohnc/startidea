import BaseComponent from "./";

export default class BaseContextComponent extends BaseComponent {
  setCurrentUserContext(currentUser) {
    this.context.setCurrentUser(currentUser);
  }

  setCurrentStoreContext(currentStore) {
    this.context.setCurrentStore(currentStore);
  }

  getCurrentUserContext() {
    return this.context.currentUser;
  }

  getCurrentStoreContext() {
    return this.context.currentStore;
  }
}
