import React from "react";
import PropTypes from "prop-types";
import Input from "components/Input";
import Checkbox from "components/Checkbox";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";

const propTypes = {
  size: PropTypes.oneOf(["small", "large"]),
  hasIcon: PropTypes.bool,
  as: PropTypes.elementType
};

const InputGroup = React.forwardRef(
  ({ size, className, as: Comp = "div", ...props }, ref) => {
    const prefix = "input-group";

    const classes = cx(
      className,
      prefix,
      getKeyOnly(size, `${prefix}-${size === "small" ? "sm" : "lg"}`)
    );

    return <Comp {...props} ref={ref} className={classes} />;
  }
);

const InputGroupAppend = createWithBSPrefix("input-group-append");
InputGroupAppend.displayName = "InputGroupAppend";

const InputGroupPrepend = createWithBSPrefix("input-group-prepend");
InputGroupPrepend.displayName = "InputGroupPrepend";

const InputGroupText = createWithBSPrefix("input-group-text", {
  Component: "span"
});
InputGroupText.displayName = "InputGroupText";

const InputGroupCheckbox = props => (
  <InputGroupText>
    <Checkbox {...props} content="" children="" />
  </InputGroupText>
);
InputGroupCheckbox.displayName = "InputGroupCheckbox";

const InputGroupRadio = props => (
  <InputGroupText>
    <Input {...props} type="radio" isCustom />
  </InputGroupText>
);
InputGroupRadio.displayName = "InputGroupRadio";

InputGroup.displayName = "InputGroup";
InputGroup.propTypes = propTypes;

InputGroup.Text = InputGroupText;
InputGroup.Radio = InputGroupRadio;
InputGroup.Checkbox = InputGroupCheckbox;
InputGroup.Append = InputGroupAppend;
InputGroup.Prepend = InputGroupPrepend;

export default InputGroup;
