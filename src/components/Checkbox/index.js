import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import isFalsy from "lib/utils/isFalsy";
import "./style.min.css";

const propTypes = {
  type: PropTypes.oneOf(["bold", "solid", "tick"])
};

const Checkbox = React.forwardRef(
  (
    { className, children, content, type, variant, isDisabled, ...rest },
    ref
  ) => {
    const prefix = "cust-checkbox";
    const classes = cx(
      className,
      prefix,
      getKeyOnly(type, `cust-checkbox--${type}`),
      getKeyOnly(isDisabled, `cust-checkbox--disabled`),
      getKeyOnly(variant, `cust-checkbox--${variant}`),
      getKeyOnly(isFalsy(content || children), `cust-checkbox--single`)
    );

    return (
      <label className={classes}>
        <input type="checkbox" ref={ref} {...rest} />
        {content || children}
        <span></span>
      </label>
    );
  }
);

Checkbox.displayName = "Checkbox";
Checkbox.propTypes = propTypes;

export default Checkbox;
