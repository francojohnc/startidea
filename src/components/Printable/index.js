import React from "react";
import "./styles.min.css";

export default function Printable({ children, hide }) {
  return <div className={hide ? "not-printable" : "printable"}>{children}</div>;
}
