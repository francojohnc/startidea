import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import Input from "components/Input";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import isSelectInput from "lib/utils/isSelectInput";
import isFileInput from "lib/utils/isFileInput";
import isString from "lib/utils/isString";
import { getSelectedFiles } from "./utils";

const propTypes = {
  type: PropTypes.oneOf([
    "radio",
    "checkbox",
    "switch",
    "select",
    "range",
    "file"
  ]).isRequired,
  label: PropTypes.string,
  isInline: PropTypes.bool,
  isValid: PropTypes.bool,
  isInvalid: PropTypes.bool
};

const CustomInput = React.forwardRef(
  (
    {
      type,
      size,
      label,
      htmlFor,
      isValid,
      children,
      isInline,
      isInvalid,
      dataBrowser,
      className,
      ...rest
    },
    ref
  ) => {
    const [files, setFiles] = useState(null);
    const labelHtmlFor = htmlFor || rest.id || "";
    const prefix = `custom-${type}`;
    const fileProps = {};

    const customClasses = cx(
      className,
      prefix,
      getKeyOnly(size, `${prefix}-${size === "small" ? "sm" : "lg"}`)
    );

    const isFile = isFileInput(type);
    const fileLabel = getKeyOnly(isFile, "Choose file");

    const Label = (
      <Fragment>
        {(label || (files || fileLabel)) && (
          <label className="custom-control-label" htmlFor={labelHtmlFor}>
            {files || label || fileLabel}
          </label>
        )}
      </Fragment>
    );

    if (isSelectInput(type)) {
      return (
        <Fragment>
          {Label}
          <Input
            {...rest}
            ref={ref}
            type="select"
            isValid={isValid}
            isInvalid={isInvalid}
            isCustom
          >
            {children}
          </Input>
        </Fragment>
      );
    }

    if (isFile) {
      if (isString(dataBrowser)) {
        fileProps["data-browser"] = dataBrowser;
      }

      fileProps.onChange = (e, ...args) => {
        const input = e.target;
        const { onChange, isMultiple } = rest;
        const files = getSelectedFiles(input, isMultiple);

        if (typeof onChange === "function") {
          onChange(e, ...args);
        }

        setFiles(files);
      };
    }

    const wrapperClasses = cx(
      customClasses,
      getKeyOnly(
        !isFile,
        cx("custom-control", getKeyOnly(isInline, "custom-control-inline"))
      )
    );

    return (
      <div className={wrapperClasses}>
        <Input
          {...rest}
          type={type}
          ref={ref}
          isValid={isValid}
          isInvalid={isInvalid}
          isCustom
        />
        {Label}
        {children}
      </div>
    );
  }
);

CustomInput.displayName = "CustomInput";
CustomInput.propTypes = propTypes;

export default CustomInput;
