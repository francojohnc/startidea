export const getSelectedFiles = (input, multiple) => {
  if (multiple && input.files) {
    const files = [].slice.call(input.files);

    return files.map(file => file.name).join(", ");
  }

  if (input.value.indexOf("fakepath") !== -1) {
    const parts = input.value.split("\\");

    return parts[parts.length - 1];
  }

  return input.value;
};
