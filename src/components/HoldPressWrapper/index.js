import React, { Component } from "react";
import PropTypes from "prop-types";
import isFunction from "lib/utils/isFunction";

function noop() {}

const propTypes = {
  time: PropTypes.number,
  onStartHoldPress: PropTypes.func,
  onEndHoldPress: PropTypes.func
};

const defaultProps = {
  time: 2,
  onStartHoldPress: noop,
  onEndHoldPress: noop
};

export default class HoldPressWrapper extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);
    this.state = {
      startTime: 0,
      isFired: false,
      isHolding: false,
      isDone: false,
      isEnough: false,
      clickEvent: null
    };

    this.timer = null;
    this.isUnmount = false;

    this.handleStartHoldPress = this.handleStartHoldPress.bind(this);
    this.handleEndHoldPress = this.handleEndHoldPress.bind(this);
    this.timeoutCallback = this.timeoutCallback.bind(this);
    this.handleClickCapture = this.handleClickCapture.bind(this);
  }

  componentWillUnmount() {
    this.isUnmount = true;
    clearTimeout(this.timer);
    this.timer = null;
  }

  handleStartHoldPress(e) {
    const { time, onStartHoldPress } = this.props;
    const { isDone } = this.state;

    const startTime = Date.now();
    const clickEvent = { type: "HoldPress" };

    this.setState({
      startTime,
      clickEvent,
      isHolding: true,
      isDone: false,
      isEnough: false
    });

    const timeDelay = time * 1000 + 1;

    if (isDone) {
      this.timer = setTimeout(() => {
        this.timeoutCallback(startTime);
      }, timeDelay);
    }

    if (isFunction(onStartHoldPress)) {
      onStartHoldPress(e);
    }

    document.documentElement.addEventListener(
      "mouseup",
      this.handleEndHoldPress
    );
  }

  handleEndHoldPress(e) {
    const { time, onEndHoldPress } = this.props;
    const { isDone, startTime } = this.state;

    document.documentElement.removeEventListener(
      "mouseup",
      this.handleEndHoldPress
    );

    if (isDone || this.isUnmount) {
      return;
    }

    const endTime = Date.now();
    const minDiff = time * 1000;
    const diff = endTime - startTime; // Time difference
    const isEnough = diff >= minDiff; // It has been held for enough time

    this.setState({
      isEnough,
      isHolding: false,
      isDone: true,
      clickEvent: null
    });

    if (isFunction(onEndHoldPress)) {
      onEndHoldPress(e, isEnough);
    }
  }

  handleClickCapture(e) {
    if (this.state.isEnough) e.stopPropagation();
  }

  timeoutCallback(_startTime) {
    const { onHoldPress } = this.props;
    const { isDone, startTime, clickEvent } = this.state;

    if (!isDone && _startTime === startTime) {
      if (isFunction(onHoldPress)) {
        onHoldPress(startTime, clickEvent);
        this.setState({ isHolding: false });
        return;
      }
    }
  }

  render() {
    const { className, style, children } = this.props;

    return (
      <div
        style={style}
        className={className}
        onMouseDown={this.handleStartHoldPress}
        onTouchStart={this.handleStartHoldPress}
        onMouseUp={this.handleEndHoldPress}
        onClickCapture={this.handleClickCapture}
        onTouchCancel={this.handleEndHoldPress}
        onTouchEnd={this.handleEndHoldPress}
      >
        {children}
      </div>
    );
  }
}
