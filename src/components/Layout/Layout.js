import React, {Component, createElement} from "react";
import cx from "lib/dom/classnames";
import isTypeOf from "lib/utils/isTypeOf";
import isGreaterThan from "lib/utils/isGreaterThan";
import getKeyOnly from "lib/dom/getKeyOnly";
import {LayoutProvider, LayoutContext} from "./LayoutContext";

function generator({prefix, tagName}) {
    return BasicComponent => {
        return class extends Component {
            render() {
                return (
                    <BasicComponent {...this.props} prefix={prefix} tagName={tagName}/>
                );
            }
        };
    };
}

function Basic({prefix, className, children, tagName, ...rest}) {
    return createElement(
        tagName,
        {className: cx(className, prefix), ...rest},
        children
    );
}

class BasicLayout extends Component {
    static contextType = LayoutContext;

    render() {
        const {
            className,
            prefix,
            hasSidebar,
            tagName: Tag,
            style,
            ...rest
        } = this.props;
        const {sidebars} = this.context;

        const classes = cx(
            className,
            prefix,
            getKeyOnly(
                isTypeOf(hasSidebar, "boolean")
                    ? hasSidebar
                    : isGreaterThan(sidebars.length, 0),
                `${prefix}--with-sidebar`
            )
        );

        return <Tag {...rest} className={classes}/>;
    }
}

class LayoutCompProvider extends Component {
    state = {sidebars: []};

    getSidebarHook() {
        return {
            addSidebar: id => {
                this.setState(state => ({
                    sidebars: [...state.sidebars, id]
                }));
            },
            removeSidebar: id => {
                this.setState(state => ({
                    sidebars: state.sidebars.filter(currentId => currentId !== id)
                }));
            }
        };
    }

    getValues() {
        return {
            ...this.state,
            sidebarHook: this.getSidebarHook()
        };
    }

    render() {
        return (
            <LayoutProvider value={this.getValues()}>
                <BasicLayout {...this.props} />
            </LayoutProvider>
        );
    }
}

const Layout = generator({
    prefix: "layout",
    tagName: "section"
})(LayoutCompProvider);
Layout.displayName = "Layout";

const Header = generator({
    prefix: "layout__header",
    tagName: "header"
})(Basic);
Header.displayName = "HeaderLayout";

const Footer = generator({
    prefix: "layout__footer",
    tagName: "footer"
})(Basic);
Header.displayName = "FooterLayout";

const Content = generator({
    prefix: "layout__content",
    tagName: "main"
})(Basic);
Header.displayName = "ContentLayout";

Layout.Header = Header;
Layout.Footer = Footer;
Layout.Content = Content;

export default Layout;
