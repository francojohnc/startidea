import Layout from "./Layout";
import Sidebar from "./Sidebar";
import "./style.min.css";

Layout.Sidebar = Sidebar;
export default Layout;
