import React, {Component, createContext, createRef} from "react";
import PropTypes from "prop-types";
import {LayoutConsumer} from "./LayoutContext";
import cx from "lib/dom/classnames";
import hasOwn from "lib/utils/hasOwn";
import omit from "lib/utils/omit";
import isNumeric from "lib/utils/isNumeric";
import {EventListener, documentRef} from "lib/dom/react-event-listener";
import Ref from "lib/dom/react-ref/Ref";
import doesNodeContainClick from "components/global/doesNodeContainClick";
import isNull from "lib/utils/isNull";
import getKeyOnly from "lib/dom/getKeyOnly";

if (typeof window !== "undefined") {
    const matchMediaPolyfill = mediaQuery => {
        return {
            media: mediaQuery,
            matches: false,
            addListener() {
            },
            removeListener() {
            }
        };
    };
    window.matchMedia = window.matchMedia || matchMediaPolyfill;
}

export const SidebarContext = createContext({});
export const SidebarCosumer = SidebarContext.Consumer;

const dimensionMap = {
    xs: "480px",
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px",
    xxl: "1600px"
};

const generateId = (() => {
    let i = 0;

    return (prefix = "") => {
        i += 1;
        return `${prefix}${i}`;
    };
})();

const propTypes = {
    isCollapsible: PropTypes.bool,
    isCollapsed: PropTypes.bool,
    isDefaultCollapsed: PropTypes.bool,
    isReverseArrow: PropTypes.bool,
    isPushable: PropTypes.bool,
    onCollapse: PropTypes.func,
    trigger: PropTypes.node,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    collapsedWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    breakpoint: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl", "xxl"]),
    onBreakpoint: PropTypes.func
};

const defaultProps = {
    isCollapsible: false,
    isDefaultCollapsed: false,
    isReverseArrow: false,
    isPushable: false,
    width: 200,
    collapsedWidth: 80,
    style: {}
};

class InternalSidebar extends Component {
    static propTypes = propTypes;
    static defaultProps = defaultProps;

    static getDerivedStateFromProps(nextProps) {
        if (hasOwn(nextProps, "isCollapsed")) {
            return {
                isCollapsed: nextProps.isCollapsed
            };
        }

        return null;
    }

    mql; // MediaQueryList
    uniqueId;
    ref = createRef();

    constructor(props) {
        super(props);

        this.uniqueId = generateId("layout__sidebar-");

        const matchMedia = window.matchMedia;

        if (props.breakpoint && props.breakpoint in dimensionMap) {
            this.mql = matchMedia(`(max-width: ${dimensionMap[props.breakpoint]})`);
        }

        this.state = {
            isCollapsed: hasOwn(props, "isCollapsed")
                ? props.isCollapsed
                : props.isDefaultCollapsed,
            isBelow: false
        };

        this.handleDocumentClick = this.handleDocumentClick.bind(this);
        this.responsiveHandler = this.responsiveHandler.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        if (this.mql) {
            this.mql.addListener(this.responsiveHandler);
            this.responsiveHandler(this.mql);
        }

        this.props.sidebarHook && this.props.sidebarHook.addSidebar(this.uniqueId);
    }

    componentDidUpdate() {
        if (this.ref && this.props.isPushable) {
            if (this.state.isCollapsed) {
                this.ref.current.parentElement.style.left = 0;
            } else {
                this.ref.current.parentElement.style.left = `-${this.transformRawWidth(
                    this.props.width
                )}`;
            }
        }
    }

    componentWillUnmount() {
        this.mql && this.mql.removeListener(this.responsiveHandler);
        this.props.sidebarHook &&
        this.props.sidebarHook.removeSidebar(this.uniqueId);
    }

    setCollapsed(isCollapsed, type) {
        if (!hasOwn(this.props, "isCollapsed")) {
            this.setState({isCollapsed});
        }

        hasOwn(this.props, "onCollapse") &&
        this.props.onCollapse(isCollapsed, type);
    }

    responsiveHandler(mql) {
        this.setState({isBelow: mql.matches});

        hasOwn(this.props, "onBreakpoint") && this.props.onBreakpoint(mql.matches);

        this.state.isCollapsed !== mql.matches &&
        this.setCollapsed(mql.matches, "responsive");
    }

    toggle() {
        this.setCollapsed(!this.state.isCollapsed, "clickTrigger");
    }

    handleDocumentClick(e) {
        if (!doesNodeContainClick(this.ref.current, e)) {
            this.toggle();
        }
    }

    getValues() {
        return {
            isSidebarCollapsed: this.state.isCollapsed,
            collapsedWidth: this.props.collapsedWidth
        };
    }

    transformRawWidth(rawWidth) {
        return isNumeric(rawWidth) ? `${rawWidth}px` : String(rawWidth);
    }

    renderSidebar() {
        const {
            className,
            isCollapsible,
            isReverseArrow,
            isPushable,
            trigger,
            style,
            width,
            collapsedWidth,
            ...rest
        } = this.props;
        const {isCollapsed, isBelow} = this.state;

        const divProps = omit(
            [
                "isCollapsed",
                "isDefaultCollapsed",
                "onCollapse",
                "breakpoint",
                "onBreakpoint",
                "sidebarHook",
                "isSidebarCollapsed",
                "toggleSidebar"
            ],
            rest
        );

        if (isCollapsed) {
            documentRef.current.body.classList.add("no-overflow-x");
        } else {
            documentRef.current.body.classList.remove("no-overflow-x");
        }

        const prefix = "layout__sidebar";

        const rawWidth = isCollapsed && !isPushable ? collapsedWidth : width;
        // use "px" as fallback unit for width
        const sidebarWidth = this.transformRawWidth(rawWidth);
        // special trigger when collapsedWidth == 0
        const zeroWidthTrigger =
            parseFloat(String(collapsedWidth || 0)) === 0 ? (
                <span
                    onClick={this.toggle}
                    className={`${prefix}__zero-width--trigger ${prefix}__zero-width--trigger--${
                        isReverseArrow ? "right" : "left"
                        }`}
                >
          <i className="la la-bars"/>
        </span>
            ) : null;

        const iconObj = {
            expanded: isReverseArrow ? (
                <i class="la la-angle-right"/>
            ) : (
                <i class="la la-angle-left"/>
            ),
            collapsed: isReverseArrow ? (
                <i class="la la-angle-left"/>
            ) : (
                <i class="la la-angle-right"/>
            )
        };

        const status = this.state.isCollapsed ? "collapsed" : "expanded";
        const defaultTrigger = iconObj[status];
        const triggerDom = !isNull(trigger)
            ? zeroWidthTrigger || (
            <div
                className={`${prefix}--trigger`}
                onClick={this.toggle}
                style={{width: sidebarWidth}}
            >
                {trigger || defaultTrigger}
            </div>
        )
            : null;

        const divStyle = {
            ...style,
            flex: `0 0 ${sidebarWidth}`,
            maxWidth: sidebarWidth,
            minWidth: sidebarWidth,
            width: sidebarWidth
        };

        const sidebarCls = cx(
            className,
            prefix,
            "layout__sidebar--light",
            getKeyOnly(isPushable, `${prefix}--pushable`),
            getKeyOnly(!!isCollapsed, `${prefix}--collapsed`),
            getKeyOnly(
                isCollapsible && !isNull(trigger) && !zeroWidthTrigger,
                `${prefix}--has-trigger`
            ),
            getKeyOnly(!!isBelow, `${prefix}--below`),
            getKeyOnly(parseFloat(sidebarWidth) === 0, `${prefix}__zero-width`)
        );

        return (
            <aside className={sidebarCls} {...divProps} style={divStyle}>
                <div className={`${prefix}__children`}>{this.props.children}</div>
                {isCollapsible || (isBelow && zeroWidthTrigger) ? triggerDom : null}
            </aside>
        );
    }

    render() {
        return (
            <Ref innerRef={this.ref}>
                <SidebarContext.Provider value={this.getValues()}>
                    {this.renderSidebar()}
                    {this.state.isCollapsed && (
                        <EventListener
                            listener={this.handleDocumentClick}
                            targetRef={documentRef}
                            type="click"
                        />
                    )}
                </SidebarContext.Provider>
            </Ref>
        );
    }
}

export default function Sidebar(props) {
    return (
        <LayoutConsumer>
            {context => <InternalSidebar {...context} {...props} />}
        </LayoutConsumer>
    );
}
