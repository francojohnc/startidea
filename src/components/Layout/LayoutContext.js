import { createContext } from "react";

export const LayoutContext = createContext({
  sidebarHook: {
    addSidebar: () => null,
    removeSidebar: () => null
  }
});

export const LayoutProvider = LayoutContext.Provider;
export const LayoutConsumer = LayoutContext.Consumer;
