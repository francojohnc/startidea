import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const showDialog = ({ customClass = {}, ...rest } = {}) => {
  const _options = {
    title: null,
    toast: false,
    allowOutsideClick: true,
    allowEscapeKey: true,
    allowEnterKey: true,
    buttonsStyling: false,
    customClass: {
      popup: "br-0",
      content: "content-wrapper",
      confirmButton: "btn btn-primary",
      cancelButton: "btn btn-secondary ml-2",
      input: "form-control form-control-lg br-0",
      ...customClass
    },
    ...rest
  };

  if (_options.toast === true) {
    // remove incompatible props with toast
    delete _options.allowOutsideClick;
    delete _options.allowEscapeKey;
    delete _options.allowEnterKey;

    // no body content
    // to avoid error
    _options.html = null;

    // use title as message
    // to avoid error
    _options.title = _options.message;

    // apply normally coupled options
    _options.timer = _options.timer || 3000;
    _options.position = _options.position || "bottom-start";
    _options.showConfirmButton = _options.showConfirmButton || false;
    _options.showCancelButton = _options.showCancelButton || false;
  } else {
    _options.html = _options.message;
  }

  // delete this property
  // because this is not a prop
  // of SweetAlert2
  delete _options.message;

  const alert = withReactContent(Swal);

  if (_options.mixin) {
    const { mixin } = _options;
    delete _options.mixin;

    alert.mixin(mixin);
  }

  if (Array.isArray(_options.queue)) {
    const { queue } = _options;
    delete _options.queue;

    return alert
      .mixin({
        ..._options
      })
      .queue(queue);
  }

  return alert.fire({
    ..._options
  });
};

export const showSuccessDialog = (message, options = {}) => {
  if (typeof options === "boolean") {
    options = { toast: options };
  } else if (options && !options.title) {
    options.title = "Great!";
  }

  return showDialog({
    title: "Great!",
    ...options,
    type: "success",
    message
  });
};

export const showErrorDialog = (message, options = {}) => {
  if (typeof options === "boolean") {
    options = { toast: options };
  } else if (options && !options.title) {
    options.title = "Oops...";
  }

  return showDialog({
    title: "Oops...",
    ...options,
    type: "error",
    message
  });
};

export const showWarningDialog = (message, options = {}) => {
  if (typeof options === "boolean") {
    options = { toast: options };
  } else if (options && !options.title) {
    options.title = "Wait!";
  }

  return showDialog({
    title: "Wait!",
    ...options,
    type: "warning",
    message
  });
};

export const showInfoDialog = (message, options = {}) => {
  if (typeof options === "boolean") {
    options = { toast: options };
  }

  return showDialog({
    title: "",
    type: "info",
    message,
    ...options
  });
};

export const showQuestionDialog = (message, options = {}) => {
  if (typeof options === "boolean") {
    options = { toast: options };
  }

  return showDialog({
    title: "",
    ...options,
    type: "question",
    showCancelButton: true,
    message
  });
};
