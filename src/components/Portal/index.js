import { Component } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";
import canUseDOM from "lib/dom/canUseDOM";

const propTypes = {
  children: PropTypes.node.isRequired,
  node: PropTypes.any
};

export default class Portal extends Component {
  static propTypes = propTypes;

  componentWillUnmount() {
    if (this.defaultNode) {
      document.body.removeChild(this.defaultNode);
    }

    this.defaultNode = null;
  }

  render() {
    if (!canUseDOM()) {
      return null;
    }

    if (!this.props.node && !this.defaultNode) {
      this.defaultNode = document.createElement("div");
      document.body.appendChild(this.defaultNode);
    }

    return createPortal(
      this.props.children,
      this.props.node || this.defaultNode
    );
  }
}
