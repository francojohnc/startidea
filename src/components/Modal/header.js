import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import Icon from "components/Icon";

const propTypes = {
  as: PropTypes.elementType,
  wrapTag: PropTypes.elementType,
  toggle: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node,
  content: PropTypes.string,
  closeAriaLabel: PropTypes.string,
  charCode: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  close: PropTypes.object
};

const defaultProps = {
  as: "h5",
  wrapTag: "div",
  closeAriaLabel: "Close",
  charCode: 215
};

const ModalHeader = props => {
  let closeButton;
  const {
    className,
    children,
    content,
    toggle,
    as: Comp,
    wrapTag: WrapTag,
    closeAriaLabel,
    charCode,
    close,
    ...rest
  } = props;

  const classes = cx(className, "modal-header");

  if (!close && toggle) {
    closeButton = (
      <button
        className="close"
        type="button"
        tabIndex="-1"
        onClick={toggle}
        aria-label={closeAriaLabel}
      >
        <span aria-hidden="true">
          <Icon name="close" />
        </span>
      </button>
    );
  }

  return (
    <WrapTag {...rest} className={classes}>
      <Comp className="modal-title">{children || content}</Comp>
      {close || closeButton}
    </WrapTag>
  );
};

ModalHeader.displayName = "ModalHeader";
ModalHeader.propTypes = propTypes;
ModalHeader.defaultProps = defaultProps;

export default ModalHeader;
