import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getOriginalBodyPadding from "lib/dom/getOriginalBodyPadding";
import conditionallyUpdateScrollbar from "lib/dom/conditionallyUpdateScrollbar";
import setBodyClassName from "lib/dom/setBodyClassName";
import setBodyScrollbarWidth from "lib/dom/setBodyScrollbarWidth";
import getBodyClassName from "lib/dom/getBodyClassName";
import getFocusableElements from "lib/dom/getFocusableElements";
import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import omit from "lib/utils/omit";
import Portal from "../Portal";
import Fade, { FadeDefaultProps } from "../Fade";
import ModalHeader from "./header";
import isString from "lib/utils/isString";
import "./styles.min.css";

function noop() {}

const propTypes = {
  isOpen: PropTypes.bool,
  isAutoFocus: PropTypes.bool,
  isCentered: PropTypes.bool,
  isScrollable: PropTypes.bool,
  isKeyboard: PropTypes.bool,
  size: PropTypes.oneOf(["large", "small", "extraLarge"]),
  toggle: PropTypes.func,
  role: PropTypes.string,
  labelledBy: PropTypes.string,
  backdrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["static"])]),
  onEnter: PropTypes.func,
  onExit: PropTypes.func,
  onOpened: PropTypes.func,
  onClosed: PropTypes.func,
  children: PropTypes.node,
  className: PropTypes.string,
  wrapClassName: PropTypes.string,
  modalClassName: PropTypes.string,
  backdropClassName: PropTypes.string,
  contentClassName: PropTypes.string,
  external: PropTypes.node,
  fade: PropTypes.bool,
  zIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  unmountOnClose: PropTypes.bool,
  returnFocusAfterClose: PropTypes.bool
};

const propKeys = Object.keys(propTypes);

const defaultProps = {
  isOpen: false,
  isAutoFocus: true,
  isCentered: false,
  isScrollable: false,
  role: "dialog",
  backdrop: true,
  isKeyboard: true,
  zIndex: 1050,
  fade: true,
  onOpened: noop,
  onClosed: noop,
  unmountOnClose: true,
  returnFocusAfterClose: true
};

const config = {
  modalTransition: {
    timeout: 150
  },
  backdropTransition: {
    mountOnEnter: true,
    timeout: 150
  }
};

const getSize = size => {
  if (!isString(size)) {
    return "";
  }

  switch (size.toLowerCase()) {
    case "small":
      return "sm";
    case "large":
      return "lg";
    case "extralarge":
      return "xl";
    default:
      return "";
  }
};

class Modal extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;
  static openCount = 0;

  constructor(props) {
    super(props);

    this.ref = React.createRef();
    this._element = null;
    this._originalBodyPadding = null;
    this.getFocusableChildren = this.getFocusableChildren.bind(this);
    this.handleBackdropClick = this.handleBackdropClick.bind(this);
    this.handleBackdropMouseDown = this.handleBackdropMouseDown.bind(this);
    this.handleEscape = this.handleEscape.bind(this);
    this.handleTab = this.handleTab.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onClosed = this.onClosed.bind(this);
    this.manageFocusAfterClose = this.manageFocusAfterClose.bind(this);

    this.state = {
      isOpen: false
    };
  }

  componentDidMount() {
    const { isOpen, isAutoFocus, onEnter } = this.props;

    if (isOpen) {
      this.init();
      this.setState({ isOpen: true });

      if (isAutoFocus) {
        this.setFocus();
      }
    }

    if (onEnter) {
      onEnter();
    }

    this._isMounted = true;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isOpen && !prevProps.isOpen) {
      this.init();
      this.setState({ isOpen: true });
      // let render() renders Modal Dialog first
      return;
    }

    // now Modal Dialog is rendered and we can refer this._element and this._dialog
    if (this.props.isAutoFocus && this.state.isOpen && !prevState.isOpen) {
      this.setFocus();
    }

    if (this._element && prevProps.zIndex !== this.props.zIndex) {
      this._element.style.zIndex = this.props.zIndex;
    }
  }

  componentWillUnmount() {
    if (this.props.onExit) {
      this.props.onExit();
    }

    if (this._element) {
      this.destroy();

      if (this.state.isOpen) {
        this.close();
      }
    }

    this._isMounted = false;
  }

  init() {
    try {
      this._triggeringElement = document.activeElement;
    } catch (err) {
      this._triggeringElement = null;
    }

    if (!this._element) {
      this._element = document.createElement("div");
      this._element.setAttribute("tabindex", "-1");
      this._element.style.position = "relative";
      this._element.style.zIndex = this.props.zIndex;
      document.body.appendChild(this._element);
    }

    this._originalBodyPadding = getOriginalBodyPadding();
    conditionallyUpdateScrollbar();

    if (Modal.openCount === 0) {
      setBodyClassName(cx("modal-open", document.body.className));
    }

    Modal.openCount += 1;
  }

  destroy() {
    if (this._element) {
      document.body.removeChild(this._element);
      this._element = null;
    }

    this.manageFocusAfterClose();
  }

  manageFocusAfterClose() {
    if (this._triggeringElement) {
      const { returnFocusAfterClose } = this.props;
      if (this._triggeringElement.focus && returnFocusAfterClose)
        this._triggeringElement.focus();
      this._triggeringElement = null;
    }
  }

  close() {
    if (Modal.openCount <= 1) {
      const modalOpenClassName = "modal-open";
      // Use regex to prevent matching `modal-open` as part of a different class, e.g. `my-modal-opened`
      const modalOpenClassNameRegex = new RegExp(
        `(^| )${modalOpenClassName}( |$)`
      );

      setBodyClassName(
        getBodyClassName()
          .replace(modalOpenClassNameRegex, " ")
          .trim()
      );
    }

    this.manageFocusAfterClose();
    Modal.openCount = Math.max(0, Modal.openCount - 1);

    setBodyScrollbarWidth(this._originalBodyPadding);
  }

  onOpened(node, isAppearing) {
    this.props.onOpened();
    (config.modalTransition.onEntered || noop)(node, isAppearing);
  }

  onClosed(node) {
    const { unmountOnClose } = this.props;
    // so all methods get called before it is unmounted
    this.props.onClosed();
    (config.modalTransition.onExited || noop)(node);

    if (unmountOnClose) {
      this.destroy();
    }
    this.close();

    if (this._isMounted) {
      this.setState({ isOpen: false });
    }
  }

  setFocus() {
    if (
      this._dialog &&
      this._dialog.parentNode &&
      typeof this._dialog.parentNode.focus === "function"
    ) {
      this._dialog.parentNode.focus();
    }
  }

  getFocusableChildren() {
    return this._element.querySelectorAll(getFocusableElements().join(", "));
  }

  getFocusedChild() {
    let currentFocus;
    const focusableChildren = this.getFocusableChildren();

    try {
      currentFocus = document.activeElement;
    } catch (err) {
      currentFocus = focusableChildren[0];
    }

    return currentFocus;
  }

  // not mouseUp because scrollbar fires it, shouldn't close when user scrolls
  handleBackdropClick(e) {
    e.stopPropagation();

    if (e.target === this._mouseDownElement) {
      if (!this.props.isOpen || this.props.backdrop !== true) return;

      const backdrop = this._dialog ? this._dialog.parentNode : null;

      if (backdrop && e.target === backdrop && this.props.toggle) {
        this.props.toggle(e);
      }
    }
  }

  handleTab(e) {
    if (e.which !== 9) return;

    const focusableChildren = this.getFocusableChildren();
    const totalFocusable = focusableChildren.length;
    if (totalFocusable === 0) return;
    const currentFocus = this.getFocusedChild();

    let focusedIndex = 0;

    for (let i = 0; i < totalFocusable; i += 1) {
      if (focusableChildren[i] === currentFocus) {
        focusedIndex = i;
        break;
      }
    }

    if (e.shiftKey && focusedIndex === 0) {
      e.preventDefault();
      focusableChildren[totalFocusable - 1].focus();
    } else if (!e.shiftKey && focusedIndex === totalFocusable - 1) {
      e.preventDefault();
      focusableChildren[0].focus();
    }
  }

  handleBackdropMouseDown(e) {
    this._mouseDownElement = e.target;
  }

  handleEscape(e) {
    if (
      this.props.isOpen &&
      this.props.isKeyboard &&
      e.keyCode === 27 &&
      this.props.toggle
    ) {
      e.preventDefault();
      e.stopPropagation();
      this.props.toggle(e);
    }
  }

  renderModalDialog() {
    const unhandledProps = omit(propKeys, this.props);
    const dialogBaseClass = "modal-dialog";

    return (
      <div
        {...unhandledProps}
        className={cx(dialogBaseClass, this.props.className, {
          [`modal-${getSize(this.props.size)}`]: this.props.size,
          [`${dialogBaseClass}-centered`]: this.props.isCentered,
          [`${dialogBaseClass}-scrollable`]: this.props.isScrollable
        })}
        role="document"
        ref={ref => (this._dialog = ref)}
      >
        <div className={cx("modal-content", this.props.contentClassName)}>
          {this.props.children}
        </div>
      </div>
    );
  }

  render() {
    const { unmountOnClose } = this.props;

    if (!!this._element && (this.state.isOpen || !unmountOnClose)) {
      const isModalHidden =
        !!this._element && !this.state.isOpen && !unmountOnClose;
      this._element.style.display = isModalHidden ? "none" : "block";

      const {
        wrapClassName,
        modalClassName,
        backdropClassName,
        isOpen,
        backdrop,
        role,
        labelledBy,
        external
      } = this.props;

      const modalAttributes = {
        onClick: this.handleBackdropClick,
        onMouseDown: this.handleBackdropMouseDown,
        onKeyUp: this.handleEscape,
        onKeyDown: this.handleTab,
        style: { display: "block" },
        "aria-labelledby": labelledBy,
        role,
        tabIndex: "-1"
      };

      const hasTransition = this.props.fade;
      const modalTransition = {
        ...FadeDefaultProps,
        ...config.modalTransition,
        baseClass: hasTransition ? config.modalTransition.baseClass : "",
        timeout: hasTransition ? config.modalTransition.timeout : 0
      };
      const backdropTransition = {
        ...FadeDefaultProps,
        ...config.backdropTransition,
        baseClass: hasTransition ? config.backdropTransition.baseClass : "",
        timeout: hasTransition ? config.backdropTransition.timeout : 0
      };

      const Backdrop =
        backdrop &&
        (hasTransition ? (
          <Fade
            {...backdropTransition}
            in={isOpen && !!backdrop}
            className={cx("modal-backdrop", backdropClassName)}
          />
        ) : (
          <div className={cx("modal-backdrop", "show", backdropClassName)} />
        ));

      return (
        <Portal node={this._element}>
          <div className={wrapClassName}>
            <Fade
              {...modalAttributes}
              {...modalTransition}
              in={isOpen}
              onEntered={this.onOpened}
              onExited={this.onClosed}
              className={cx("modal", modalClassName)}
            >
              {external}
              {this.renderModalDialog()}
            </Fade>
            {Backdrop}
          </div>
        </Portal>
      );
    }

    return null;
  }
}

Modal.Header = ModalHeader;
Modal.Body = createWithBSPrefix("modal-body");
Modal.Footer = createWithBSPrefix("modal-footer");

export default Modal;
