import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import isString from "lib/utils/isString";
import getKeyOnly from "lib/dom/getKeyOnly";
import "./styles.min.css";

Spinner.propTypes = {
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "info",
    "warning",
    "danger",
    "dark",
    "light"
  ]),
  size: PropTypes.oneOf(["small", "medium", "large", "extra-large"])
};

Spinner.defaultProps = {
  size: "medium",
  variant: "primary"
};

const getSize = size => {
  if (!isString(size)) {
    return "";
  }

  switch (size.toLowerCase()) {
    case "small":
      return "sm";
    case "large":
      return "lg";
    case "extra-large":
      return "xl";
    default:
      return "md";
  }
};

function Spinner({ className, variant, size, ...rest }) {
  const prefix = "custom-spinner";
  const classes = cx(
    className,
    prefix,
    getKeyOnly(variant, `${prefix}-${variant}`),
    getKeyOnly(size, `${prefix}-${getSize(size)}`)
  );

  return <div aria-labelledby="loading" {...rest} className={classes} />;
}

export default Spinner;
