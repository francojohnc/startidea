import React, { forwardRef } from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import pick from "lib/utils/pick";
import omit from "lib/utils/omit";
import Transition, { TransitionPropTypes } from "components/Transition";

export const TransitionPropTypeKeys = [
  "in",
  "mountOnEnter",
  "unmountOnExit",
  "appear",
  "enter",
  "exit",
  "timeout",
  "onEnter",
  "onEntering",
  "onEntered",
  "onExit",
  "onExiting",
  "onExited"
];

export const FadePropTypes = {
  ...TransitionPropTypes,
  as: PropTypes.elementType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  baseClass: PropTypes.string,
  baseClassActive: PropTypes.string,
  className: PropTypes.string,
  innerRef: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.func
  ])
};

export const FadeDefaultProps = {
  ...Transition.defaultProps,
  as: "div",
  baseClass: "fade",
  baseClassActive: "show",
  timeout: 150,
  appear: true,
  enter: true,
  exit: true,
  in: true
};

const Fade = forwardRef(
  (
    {
      as: Comp,
      baseClass,
      baseClassActive,
      className,
      children,
      ...otherProps
    },
    ref
  ) => {
    const transitionProps = pick(TransitionPropTypeKeys, otherProps);
    const childProps = omit(TransitionPropTypeKeys, otherProps);

    return (
      <Transition {...transitionProps}>
        {status => {
          const isActive = status === "entered";
          const classes = cx(className, baseClass, isActive && baseClassActive);

          return (
            <Comp {...childProps} className={classes} ref={ref}>
              {children}
            </Comp>
          );
        }}
      </Transition>
    );
  }
);

Fade.propTypes = FadePropTypes;
Fade.defaultProps = FadeDefaultProps;

export default Fade;
