import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import getElementType from "lib/dom/getElementType";

PlaceholderHeader.propTypes = {
  as: PropTypes.elementType,
  children: PropTypes.node,
  className: PropTypes.string,
  content: PropTypes.string,
  image: PropTypes.bool
};

function PlaceholderHeader({
  as,
  children,
  className,
  content,
  image,
  ...rest
}) {
  const classes = cx(getKeyOnly(image, "image"), "header", className);
  const ElementType = getElementType(PlaceholderHeader, { as });

  return (
    <ElementType {...rest} className={classes}>
      {children || content}
    </ElementType>
  );
}

export default PlaceholderHeader;
