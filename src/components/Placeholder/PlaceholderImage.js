import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import getElementType from "lib/dom/getElementType";

PlaceholderImage.propTypes = {
  as: PropTypes.elementType,
  className: PropTypes.string,
  square: PropTypes.bool,
  rectangular: PropTypes.bool
};

function PlaceholderImage({ as, className, square, rectangular, ...rest }) {
  const classes = cx(
    getKeyOnly(square, "square"),
    getKeyOnly(rectangular, "rectangular"),
    "image",
    className
  );
  const ElementType = getElementType(PlaceholderImage, { as });

  return <ElementType {...rest} className={classes} />;
}

export default PlaceholderImage;
