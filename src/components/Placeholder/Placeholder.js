import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getKeyOnly from "lib/dom/getKeyOnly";
import getElementType from "lib/dom/getElementType";
import PlaceholderHeader from "./PlaceholderHeader";
import PlaceholderImage from "./PlaceholderImage";
import PlaceholderLine from "./PlaceholderLine";
import PlaceholderParagraph from "./PlaceholderParagraph";
import "./style.min.css";

Placeholder.propTypes = {
  as: PropTypes.elementType,
  children: PropTypes.node,
  className: PropTypes.string,
  content: PropTypes.string,
  fluid: PropTypes.bool
};

function Placeholder({ as, children, className, content, fluid, ...rest }) {
  const classes = cx(
    "custom",
    "placeholder",
    getKeyOnly(fluid, "fluid"),
    className
  );
  const ElementType = getElementType(Placeholder, { as });

  return (
    <ElementType {...rest} className={classes}>
      {children || content}
    </ElementType>
  );
}

Placeholder.Header = PlaceholderHeader;
Placeholder.Image = PlaceholderImage;
Placeholder.Line = PlaceholderLine;
Placeholder.Paragraph = PlaceholderParagraph;

export default Placeholder;
