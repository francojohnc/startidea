import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getElementType from "lib/dom/getElementType";

PlaceholderParagraph.propTypes = {
  as: PropTypes.elementType,
  children: PropTypes.node,
  className: PropTypes.string,
  content: PropTypes.string
};

function PlaceholderParagraph({ as, children, className, content, ...rest }) {
  const classes = cx("paragraph", className);
  const ElementType = getElementType(PlaceholderParagraph, { as });

  return (
    <ElementType {...rest} className={classes}>
      {children || content}
    </ElementType>
  );
}

export default PlaceholderParagraph;
