import React from "react";
import PropTypes from "prop-types";
import cx from "lib/dom/classnames";
import getElementType from "lib/dom/getElementType";

PlaceholderLine.propTypes = {
  as: PropTypes.elementType,
  className: PropTypes.string,
  length: PropTypes.oneOf([
    "full",
    "very long",
    "long",
    "medium",
    "short",
    "very short"
  ])
};

function PlaceholderLine({ as, className, length, ...rest }) {
  const classes = cx("line", length, className);
  const ElementType = getElementType(PlaceholderLine, { as });

  return <ElementType {...rest} className={classes} />;
}

export default PlaceholderLine;
