import createWithBSPrefix from "lib/dom/createWithBSPrefix";
import "./styels.min.css";

export const Tab = createWithBSPrefix("tab");
export const Tabs = createWithBSPrefix("tabs");
export const TabContent = createWithBSPrefix("tab-content");
