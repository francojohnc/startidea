import Parse from "parse";
import {from} from "rxjs";

export default function () {
    let query = new Parse.Query('Product');
    return from(query.find());
}
