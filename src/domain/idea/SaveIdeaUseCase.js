import {from} from "rxjs";
import {getNewObject, getReference} from "lib/shared/parseUtils";

export default function ({name, productId, description,domain}) {
    let app = getNewObject("Idea");
    app.set("name", name);
    app.set("product", getReference("Product", productId));
    app.set("description", description);
    app.set("domain", domain);
    return from(app.save());
}
