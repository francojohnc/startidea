import Parse from "parse";
import {from} from "rxjs";

export default function (ideaId) {
    return from(Parse.Cloud.run('generate', {objectId: ideaId}));
}
