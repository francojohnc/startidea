import Parse from "parse";
import {Observable} from "rxjs";

export default function () {
    const observable = new Observable(emitter => {
        Parse.User.logOut();
        emitter.next();
        emitter.complete();
    });
    return observable;
}
