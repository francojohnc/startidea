import Parse from "parse";
import { from } from "rxjs";

export default function(username, password) {
  let user = Parse.User.logIn(username, password);
  return from(user);
}
