import Parse from "parse";
import {Observable} from "rxjs";

export default function () {
    const observable = new Observable(emitter => {
        let user = Parse.User.current();

        if (user) {
            emitter.next(user);
            emitter.complete();
        } else {
            emitter.error("No User Found");
        }
    });

    return observable;
}
