import Parse from "parse";
import {from} from "rxjs";

export default function ({username, password, ...res}) {
    let user = new Parse.User();
    user.set("username", username ? username : res.email);
    user.set("password", password);
    //other data
    for (let key in res) {
        let value = res[key];
        user.set(key, value);
    }
    return from(user.signUp());
}
