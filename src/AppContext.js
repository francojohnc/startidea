import React, {Component, createContext} from "react";

const AppContext = createContext();

export class AppProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: undefined,
        };
    }

    setCurrentUser = currentUser => {
        this.setState({currentUser});
    }


    getValue = () => ({
        ...this.state,
        setCurrentUser: this.setCurrentUser,
    });

    render() {
        return (
            <AppContext.Provider value={this.getValue()}>
                {this.props.children}
            </AppContext.Provider>
        );
    }
}

export const withContext = WrappedComponent => {
    return React.forwardRef((props, ref) => (
        <AppContext.Consumer>
            {value => <WrappedComponent ref={ref} {...value} {...props} />}
        </AppContext.Consumer>
    ));
};

export default AppContext;
