export default val => !isNaN(parseFloat(val)) && isFinite(val);
