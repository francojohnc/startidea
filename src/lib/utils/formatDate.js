import { isValidDate, toDate } from "./date";

export default date => {
  return isValidDate(date)
    ? toDate(date).toLocaleDateString("en-US", { dateStyle: "medium" })
    : "";
};
