import isTrue from "./isTrue";
import hasEvery from "./hasEvery";

export default (...args) => isTrue(hasEvery(...args));
