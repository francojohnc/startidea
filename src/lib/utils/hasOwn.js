export default (object, key) =>
  !!object ? hasOwnProperty.call(object, key) || object[key] : false;
