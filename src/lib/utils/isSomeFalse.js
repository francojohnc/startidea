import isFalse from "./isFalse";
import hasSome from "./hasSome";

export default (...args) => isFalse(hasSome(...args));
