import isArray from "./isArray";
import testRegex from "./testRegex";

const reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/;
const reIsPlainProp = /^\w*$/;

export default val =>
  isArray(val)
    ? false
    : testRegex(reIsPlainProp, val) || !testRegex(reIsDeepProp, val);
