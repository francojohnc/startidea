import isGreaterThan from "./isGreaterThan";
import testRegex from "./testRegex";

const reIsUint = /^(?:0|[1-9]\d*)$/;

export default val => testRegex(reIsUint, val) && isGreaterThan(val, -1);
