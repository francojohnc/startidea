import isKey from "./isKey";
import stringToPath from "./stringToPath";
import isObject from "./isObject";
import isArray from "./isArray";
import isIndex from "./isIndex";

export default (obj, path, val) => {
    let i = -1;
    const tempPath = isKey(path) ? [path] : stringToPath(path);
    const length = tempPath.length;
    const lastIndex = length - 1;

    while (++i < length) {
        const key = tempPath[i];
        let newVal = val;

        if (i !== lastIndex) {
            const objVal = obj[key];

            if (isObject(objVal) || isArray(objVal)) {
                newVal = objVal;
            } else if (isIndex(tempPath[i + 1])) {
                newVal = [];
            } else {
                newVal = {};
            }
        }

        obj[key] = newVal;
        obj = obj[key];
    }

    return obj;
};
