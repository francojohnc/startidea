export default num => Math.round((num + 0.00001) * 100) / 100;
