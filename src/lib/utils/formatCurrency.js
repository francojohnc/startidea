import isString from "./isString";
import toNumber from "./toNumber";

export default number => {
  if (isString(number)) {
    number = toNumber(number);
  }

  return number
    .toLocaleString("en-US", {
      style: "currency",
      currency: "PHP",
      minimumFractionDigits: 2
    })
    .replace("PHP", "")
    .trimStart();
};
