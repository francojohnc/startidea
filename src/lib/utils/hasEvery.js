import isFunction from "./isFunction";

export default (...args) => {
  return args.every(arg => !!(isFunction(arg) ? arg() : arg));
};
