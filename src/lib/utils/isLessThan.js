import toNumber from "./toNumber";

export default (num, limit, included = false) =>
  included ? toNumber(num) <= toNumber(limit) : toNumber(num) < toNumber(limit);
