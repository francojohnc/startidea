import isTypeOf from "./isTypeOf";

export default val => isTypeOf(val, "object");
