export default (val, strict = true) => {
  if (strict) {
    return isFinite(val) ? val * 1 : 0;
  }

  return val * 1;
};
