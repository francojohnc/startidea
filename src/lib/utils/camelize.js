let rHyphen = /-(.)/g;

export default string => string.replace(rHyphen, (_, chr) => chr.toUpperCase());
