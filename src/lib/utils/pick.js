import isArray from "./isArray";

export default function(keys, obj) {
  const pickKeys = isArray(keys) ? keys : [keys];
  const result = {};

  let { length } = pickKeys;
  let key;

  while (length > 0) {
    key = keys[--length];
    result[key] = obj[key];
  }

  return result;
}
