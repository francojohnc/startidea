import isGreaterThan from "./isGreaterThan";

export default num => isGreaterThan(num, 0);
