import isEqual from "./isEqual";

export default val => isEqual(val, null, false);
