export default (lVal, rVal, strict = true) =>
  strict ? lVal !== rVal : lVal != rVal;
