import isArray from "./isArray";
import isNil from "./isNil";
import isTypeObject from "./isTypeObject";

export default val => isTypeObject(val) && !isArray(val) && !isNil(val);
