import camelize from "./camelize";

export default str => str[0].toUpperCase() + camelize(str).slice(1);
