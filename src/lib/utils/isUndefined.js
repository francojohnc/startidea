import isEqual from "./isEqual";

export default val => isEqual(val, undefined);
