import getKeys from "./getKeys";
import isNil from "./isNil";
import isEqual from "./isEqual";

export default val => (isNil(val) ? true : isEqual(getKeys(val).length, 0));
