export function isSameDay(d1, d2) {
  const dateLeft = startOfDay(d1);
  const dateRight = startOfDay(d2);

  return dateLeft.getTime() === dateRight.getTime();
}

export function getStartOfDay(date) {
  const d = new Date(date);
  d.setHours(0, 0, 0, 0);
  return d;
}

export function getEndOfDay(date) {
  const d = new Date(date);
  d.setHours(23, 59, 59, 999);
  return d;
}

export function parseDate(date) {
  return Date.parse(date);
}

export function toDate(date) {
  const argStr = Object.prototype.toString.call(date);

  // Clone the date
  if (typeof date === "object" && argStr === "[object Date]") {
    // Prevent the date to lose the milliseconds when passed to new Date() in IE10
    return new Date(date.getTime());
  } else if (typeof date === "number" || argStr === "[object Number]") {
    return new Date(date);
  } else if (typeof date === "string" || argStr === "[object String]") {
    return new Date(parseDate(date));
  }

  return new Date(NaN);
}

export function isValidDate(date) {
  return !isNaN(toDate(date));
}

export function startOfDay(dirtyDate) {
  const date = toDate(dirtyDate);
  date.setHours(0, 0, 0, 0);
  return date;
}

export function startOfMonth(dirtyDate) {
  const date = toDate(dirtyDate);
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  return date;
}

export function endOfDay(dirtyDate) {
  const date = toDate(dirtyDate);
  date.setHours(23, 59, 59, 999);
  return date;
}

export function endOfMonth(dirtyDate) {
  const date = toDate(dirtyDate);
  const month = date.getMonth();
  date.setFullYear(date.getFullYear(), month + 1, 0);
  date.setHours(23, 59, 59, 999);
  return date;
}

export function startOfToday() {
  return startOfDay(Date.now());
}

export function endOfToday() {
  return endOfDay(Date.now());
}

export function isEqualDate(dirtyLeftDate, dirtyRightDate) {
  const dateLeft = toDate(dirtyLeftDate);
  const dateRight = toDate(dirtyRightDate);

  return dateLeft.getTime() === dateRight.getTime();
}

export function isBeforeToday(dirtyDate) {
  const date = toDate(dirtyDate);

  return isBefore(startOfDay(date), startOfToday());
}

export function isBefore(dirtyDate, dirtyDateToComapre) {
  const date = toDate(dirtyDate);
  const dateToCompare = toDate(dirtyDateToComapre);

  return date.getTime() < dateToCompare.getTime();
}

export function isAfter(dirtyDate, dirtyDateToComapre) {
  const date = toDate(dirtyDate);
  const dateToCompare = toDate(dirtyDateToComapre);

  return date.getTime() > dateToCompare.getTime();
}

export function isFuture(dirtyDate) {
  const date = toDate(dirtyDate);

  return date.getTime() > Date.now();
}

export function isPast(dirtyDate) {
  const date = toDate(dirtyDate);

  return date.getTime() < Date.now();
}
