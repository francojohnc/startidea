const rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;
const reEscapeChar = /\\(\\)?/g;

export default str => {
  const result = [];

  str.replace(rePropName, (match, number, quote) => {
    result.push(quote ? str.replace(reEscapeChar, "$1") : number || match);
  });

  return result;
};
