import isTypeOf from "./isTypeOf";

export default value => isTypeOf(value, "function");
