import isFalse from "./isFalse";
import hasEvery from "./hasEvery";

export default (...args) => isFalse(hasEvery(...args));
