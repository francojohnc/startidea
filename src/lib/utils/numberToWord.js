import isNumber from "./isNumber";
import isString from "./isString";

export const numberToWordMap = {
  1: "one",
  2: "two",
  3: "three",
  4: "four",
  5: "five",
  6: "six",
  7: "seven",
  8: "eight",
  9: "nine",
  10: "ten",
  11: "eleven",
  12: "twelve"
};

export default val =>
  isNumber(val) || isString(val) ? numberToWordMap[val] || val : "";
