export default (message, isDebug = true) => {
  if (isDebug) {
    console.error(message);
  }

  throw new Error(message);
};
