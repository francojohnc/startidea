import isArray from "./isArray";
import isString from "./isString";
import isFalsy from "./isFalsy";
import hasOwn from "./hasOwn";
import isFunction from "./isFunction";
import isUndefined from "./isUndefined";
import isObject from "./isObject";

export default function(rule) {
  const omitWhenEqual = value => key => key === value;
  const omitWhenIn = excludedKeys => key => excludedKeys.indexOf(key) >= 0;

  if (isString(rule)) {
    rule = omitWhenEqual(rule);
  }

  if (isArray(rule)) {
    rule = omitWhenIn(rule);
  }

  function isValidRule(rule, target) {
    return isFunction(rule) && !rule(target);
  }

  function omitInternal(target) {
    const copy = {};

    // in the case that we have been passed a falsey value, just return that
    if (isFalsy(target)) {
      return target;
    }

    if (isArray(target)) {
      return target.map(omitInternal).filter(v => !isUndefined(v));
    }

    if (!isObject(target)) {
      if (isValidRule(rule, target)) {
        return target;
      }

      return undefined;
    }

    for (const key in target) {
      if (hasOwn(target, key)) {
        // if we don't have a valid rule, just accept the value
        if (isValidRule(rule, key)) {
          copy[key] = target[key];
        }
      }
    }

    return copy;
  }

  return !isUndefined(arguments[1]) ? omitInternal(arguments[1]) : omitInternal;
}
