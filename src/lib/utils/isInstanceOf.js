export default (obj, constructor) => obj instanceof constructor;
