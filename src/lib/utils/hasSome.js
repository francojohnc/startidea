import isFunction from "./isFunction";

export default (...args) => {
  return args.some(arg => !!(isFunction(arg) ? arg() : arg));
};
