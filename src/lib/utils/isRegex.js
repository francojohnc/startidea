import isInstanceOf from "./isInstanceOf";

export default val => isInstanceOf(val, RegExp);
