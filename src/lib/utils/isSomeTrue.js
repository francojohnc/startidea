import isTrue from "./isTrue";
import hasSome from "./hasSome";

export default (...args) => isTrue(hasSome(...args));
