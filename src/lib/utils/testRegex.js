import isRegex from "./isRegex";

export default (regex, val) => {
  if (!isRegex(regex)) {
    throw new Error("Given expressions is invalid");
  }

  return regex.test(val);
};
