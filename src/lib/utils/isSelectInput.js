import isEqual from "./isEqual";

export default type => isEqual(type, "select");
