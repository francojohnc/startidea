import isEqual from "./isEqual";

export default (val, type) => isEqual(typeof val, type);
