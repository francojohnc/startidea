export default (lVal, rVal, strict = true) =>
  // eslint-disable-next-line
  strict ? lVal === rVal : lVal == rVal;
