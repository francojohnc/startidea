import isEqual from "./isEqual";

export default value => isEqual(value, null);
