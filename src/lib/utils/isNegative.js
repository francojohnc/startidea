import isLessThan from "./isLessThan";

export default num => isLessThan(num, 0);
