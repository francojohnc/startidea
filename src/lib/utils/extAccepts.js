import isArray from "./isArray";

export default function(file, acceptedFiles) {
  if (!file || !acceptedFiles) {
    return true;
  }

  const acceptedFilesArray = isArray(acceptedFiles)
    ? acceptedFiles
    : acceptedFiles.split(",");

  const mimeType = file.type || "";
  const fileName = (file.name || "").toLowerCase();
  const baseMimeType = mimeType.replace(/\/.*$/, "");

  return acceptedFilesArray.some(type => {
    const validType = type.trim();

    if (validType.charAt(0) === ".") {
      return fileName.endsWith(validType.toLowerCase());
    } else if (validType.endsWith("/*")) {
      // This is something like a image/* mime type
      return baseMimeType === validType.replace(/\/.*$/, "");
    }

    return mimeType === validType;
  });
}
