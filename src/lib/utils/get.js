import isUndefined from "./isUndefined";
import isNil from "./isNil";
import isEqual from "./isEqual";

export default (obj, path, defaultValue) => {
  const result = String.prototype.split
    .call(path, /[,[\].]+?/)
    .filter(Boolean)
    .reduce((res, key) => (!isNil(res) ? res[key] : res), obj);

  return isUndefined(result) || isEqual(result, obj) ? defaultValue : result;
};
