import isTypeOf from "./isTypeOf";

export default val => isTypeOf(val, "string") || val instanceof String;
