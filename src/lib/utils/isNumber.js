import isTypeOf from "./isTypeOf";

export default val => isTypeOf(val, "number") || val instanceof Number;
