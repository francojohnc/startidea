import isRadioInput from "lib/dom/isRadioInput";
import getRadioValue from "./getRadioValue";
import isMultipleSelectInput from "lib/dom/isMultipleSelectInput";
import getMutipleSelectValue from "./getMutipleSelectValue";
import isCheckBoxInput from "lib/dom/isCheckBoxInput";
import isUndefined from "lib/utils/isUndefined";

export default function getFieldValue(fields, ref) {
  const { type, name, options, checked, value, files } = ref;

  if (type === "file") {
    return files;
  }

  if (isRadioInput(type)) {
    const field = fields[name];
    return field ? getRadioValue(field.options).value : "";
  }

  if (isMultipleSelectInput(type)) return getMutipleSelectValue(options);

  if (isCheckBoxInput(type)) {
    if (checked) {
      return ref.attributes && ref.attributes.value
        ? isUndefined(value) || value === ""
          ? true
          : value
        : true;
    }

    return false;
  }

  return value;
}
