import getFieldValue from "./getFieldValue";

export default fields =>
  Object.values(fields).reduce((values, { ref, ref: { name } }) => {
    values[name] = getFieldValue(fields, ref);

    return values;
  }, {});
