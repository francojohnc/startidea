export default options =>
  [...options].reduce((values, { selected, value }) => {
    if (!selected) return values;

    values.push(value);

    return values;
  }, []);
