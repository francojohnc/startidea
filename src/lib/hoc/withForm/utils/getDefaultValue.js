import isUndefined from "lib/utils/isUndefined";
import get from "lib/utils/get";

export default (defaultValues, name, defaultValue) =>
  isUndefined(defaultValues[name])
    ? get(defaultValues, name, defaultValue)
    : defaultValues[name];
