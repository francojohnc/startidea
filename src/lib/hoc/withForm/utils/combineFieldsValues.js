import set from "lib/utils/set";

export default data =>
  Object.entries(data).reduce((values, [key, value]) => {
    if (!!key.match(/\[.+\]/gi) || key.indexOf(".") > 0) {
      set(values, key, value);
      return values;
    }

    values[key] = value;

    return values;
  }, {});
