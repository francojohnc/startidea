import onDOMRemove from "lib/dom/onDOMRemove";
import isRadioInput from "lib/dom/isRadioInput";
import hasOwn from "lib/utils/hasOwn";
import isArray from "lib/utils/isArray";

export default function registerField(ref, fields, deleteFieldRef) {
  if (!hasOwn(ref, "name")) return console.warn("Missing name on ref", ref);

  const { name, type, value } = ref;
  const fieldAttributes = { ref };
  const isRadio = isRadioInput(type);
  const currentField = fields[name];
  const isRegistered = isRadio
    ? currentField &&
      isArray(currentField.options) &&
      currentField.options.find(({ ref }) => value === ref.value)
    : currentField;

  if (isRegistered) return;

  if (!type) {
    fields[name] = fieldAttributes;
    return;
  } else {
    const mutationWatcher = onDOMRemove(ref, () => deleteFieldRef(name));

    if (!isRadio) {
      fields[name] = { ...fieldAttributes, mutationWatcher };
      return;
    }

    if (!currentField) {
      fields[name] = {
        options: [],
        ref: { type: "radio", name }
      };
    }

    fields[name].options.push({ ref, mutationWatcher });
  }
}
