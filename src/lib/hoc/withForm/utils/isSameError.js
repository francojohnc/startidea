import isObject from "lib/utils/isObject";

export default (error, type, message) =>
  isObject(error) && (error.type === type && error.message === message);
