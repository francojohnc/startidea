import isArray from "lib/utils/isArray";

const defaultReturn = {
  isValid: false,
  value: ""
};

export default (options = []) =>
  isArray(options)
    ? options.reduce(
        (previous, { ref: { checked, value } }) =>
          checked
            ? {
                isValid: true,
                value
              }
            : previous,
        defaultReturn
      )
    : defaultReturn;
