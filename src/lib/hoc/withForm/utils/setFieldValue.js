import isRadioInput from "lib/dom/isRadioInput";
import isMultipleSelectInput from "lib/dom/isMultipleSelectInput";
import isCheckBoxInput from "lib/dom/isCheckBoxInput";
import isNil from "lib/utils/isNil";

export default function setFieldValue(field, rawValue) {
  if (!field) return false;

  const ref = field.ref;
  const { type } = ref;
  const options = field.options;
  const value = ref instanceof HTMLElement && isNil(rawValue) ? "" : rawValue;

  if (isRadioInput(type) && options) {
    options.forEach(
      ({ ref: radioRef }) => (radioRef.checked = radioRef.value === value)
    );
  } else if (isMultipleSelectInput(type)) {
    [...ref.options].forEach(
      selectRef => (selectRef.selected = value.includes(selectRef.value))
    );
  } else {
    ref[isCheckBoxInput(type) ? "checked" : "value"] = value;
  }

  return type;
}
