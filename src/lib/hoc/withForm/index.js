import React, {Component} from "react";
import hoistNonReactStatics from "hoist-non-react-statics";
import isNil from "lib/utils/isNil";
import setFieldValue from "./utils/setFieldValue";
import registerField from "./utils/registerField";
import getDefaultValue from "./utils/getDefaultValue";
import getFieldsValues from "./utils/getFieldsValues";
import combineFieldsValues from "./utils/combineFieldsValues";
import isUndefined from "lib/utils/isUndefined";
import isArray from "lib/utils/isArray";
import isSameError from "./utils/isSameError";

export default function withForm(
    {useSubmissionState = true} = {useSubmissionState: true}
) {
    return function createForm(WrappedComponent) {
        class WithForm extends Component {
            constructor(props) {
                super(props);

                this.fieldsRef = {};
                this.errorsRef = {};
                this.defaultValuesRef = {};
                this.isUnMount = false;
                this.isSubmittedRef = false;
                this.isSubmittingRef = false;

                this.state = {
                    noop: {}
                };

                this.reset = this.reset.bind(this);
                this.reRender = this.reRender.bind(this);
                this.register = this.register.bind(this);
                this.setValue = this.setValue.bind(this);
                this.setError = this.setError.bind(this);
                this.setErrors = this.setErrors.bind(this);
                this.clearError = this.clearError.bind(this);
                this.handleSubmit = this.handleSubmit.bind(this);
                this.deleteFieldRef = this.deleteFieldRef.bind(this);
                this.setValueInternal = this.setValueInternal.bind(this);
            }

            componentWillUnmount() {
                this.isUnMount = true;

                this.fieldsRef &&
                Object.values(this.fieldsRef).forEach(field =>
                    this.deleteFieldRef(field)
                );
            }

            reRender() {
                this.setState({noop: {}});
            }

            deleteFieldRef(name) {
                delete this.fieldsRef[name];
                delete this.errorsRef[name];
            }

            setValueInternal(name, value) {
                setFieldValue(this.fieldsRef[name], value);
            }

            setValue(name, value) {
                this.setValueInternal(name, value);
            }

            register(ref) {
                if (isNil(window) || !ref) return;

                registerField(ref, this.fieldsRef, this.deleteFieldRef);
            }

            clearError(name) {
                if (isUndefined(name)) {
                    this.errorsRef = {};
                } else {
                    (isArray(name) ? name : [name]).forEach(
                        fieldName => delete this.errorsRef[fieldName]
                    );
                }

                this.reRender();
            }

            setErrorInternal(name, type, message, reRender = true) {
                const errors = this.errorsRef;

                if (!isSameError(errors[name], type, message)) {
                    errors[name] = {
                        type,
                        message
                    };

                    reRender && this.reRender();
                }
            }

            setError(name, type, message) {
                this.setErrorInternal(name, type, message);
            }

            setErrors(errors) {
                (isArray(errors) ? errors : [errors]).forEach(
                    ({name, type, message}) => {
                        this.setErrorInternal(name, type, message, false);
                    }
                );

                this.reRender();
            }

            handleSubmit(callback) {
                const self = this;

                return async e => {
                    if (e) {
                        e.preventDefault();
                        e.persist();
                    }

                    let fieldValues;
                    const fields = self.fieldsRef;

                    if (useSubmissionState) {
                        self.isSubmittingRef = true;
                        self.reRender();
                    }

                    fieldValues = getFieldsValues(fields);

                    await callback(combineFieldsValues(fieldValues), e);

                    if (self.isUnMount) return;

                    if (useSubmissionState) {
                        self.isSubmittedRef = true;
                        self.isSubmittingRef = false;
                        self.reRender();
                    }
                };
            }

            reset(values) {
                const fieldsKeyValue = Object.entries(this.fieldsRef);

                for (let [, value] of fieldsKeyValue) {
                    if (value && value.ref && value.ref.closest) {
                        try {
                            value.ref.closest("form").reset();
                            break;
                        } catch {
                        }
                    }
                }

                this.errorsRef = {};
                this.defaultValuesRef = {};
                this.isSubmittedRef = false;

                if (values) {
                    fieldsKeyValue.forEach(([key, field]) =>
                        setFieldValue(field, getDefaultValue(values, key))
                    );
                    this.defaultValuesRef = {...values};
                }

                this.reRender();
            }

            render() {
                return (
                    <WrappedComponent
                        {...this.props}
                        errors={this.errorsRef}
                        reset={this.reset}
                        register={this.register}
                        setValue={this.setValue}
                        setError={this.setError}
                        setErrors={this.setErrors}
                        clearError={this.clearError}
                        handleSubmit={this.handleSubmit}
                        isSubmitted={this.isSubmittedRef}
                        isSubmitting={this.isSubmittingRef}
                    />
                );
            }
        }

        return hoistNonReactStatics(WithForm, WrappedComponent);
    };
}
