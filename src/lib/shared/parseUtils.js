import Parse from "parse";
import isNil from "lib/utils/isNil";
import isEmpty from "lib/utils/isEmpty";

export const getImage = (file, name) => {
  return {
    name,
    fileName: file._name,
    url: file._url
  };
};

export const fetchByAttr = (PObj, attr) => {
  return PObj.get(attr).fetch();
};

export const fetchRelationByAttr = async (PObj, attr) => {
  return PObj.relation(attr)
    .query()
    .find();
};

export const getCurrentUserRef = () => {
  return getReference("User", Parse.User.current()._getId(), false);
};

export const getQuery = className => {
  if (isEmpty(className)) {
    return Parse.Query;
  }

  return new Parse.Query(className);
};

export const getNewObject = className => {
  return new Parse.Object(className);
};

export const getReference = (className, id, newObj = true) => {
  if (!isNil(id)) {
    return Parse.Object.extend(className).createWithoutData(id);
  }

  if (newObj) {
    return getNewObject(className);
  }

  return undefined;
};

export const createPagination = (query, pagination) => {
  // if no pagination to create
  // then pass this as fallback
  if (!pagination) {
    return query.find();
  }

  const { itemsPerPage, currentPage } = pagination;

  return Promise.all([
    query.count().then(totalItems => Math.ceil(totalItems / itemsPerPage)),
    query
      .limit(itemsPerPage)
      .skip((currentPage - 1) * itemsPerPage)
      .find()
  ]);
};

export const getValues = (obj, key) => {
  const currentValue = obj.get(key);

  obj.revert(key);
  const previousValue = obj.get(key);

  obj.set(key, currentValue);

  return [previousValue, currentValue];
};

export const getFullName = obj => {
  return (obj.get("firstName") + " " + obj.get("lastName")).trimRight();
};
