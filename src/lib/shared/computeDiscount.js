export default function computeDiscount(price, discount, discountType) {
  if (discountType === "PERCENTAGE") {
    return (discount / 100) * price;
  } else if (discountType === "FIXED") {
    return discount;
  }

  return 0;
}
