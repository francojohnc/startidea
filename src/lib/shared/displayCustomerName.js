export default function displayCustomerName(
  customer,
  contactPersonOnly = false
) {
  if (!customer) {
    return null;
  }

  if (!customer.get("companyName") || contactPersonOnly) {
    return customer.get("contactPerson") || "";
  }

  return `${customer.get("companyName")} (${customer.get("contactPerson")})`;
}
