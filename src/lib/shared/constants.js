export const DEFAULT_FORM_INITIALIZER = {
  isSubmitting: false,
  isSubmitted: false,
  values: {}
};

export const REDIRECT_TO_SIGNIN_MSG =
  "We're now redirecting you to signin page.";

export const UNREGISTERED_USER_ERR = `You are not a registered user.\n${REDIRECT_TO_SIGNIN_MSG}`;

export const UNASSIGNED_USER_ERR =
  "You are not assigned to any stores.\n" +
  "Please, contact your adminstrator.\n" +
  REDIRECT_TO_SIGNIN_MSG;

export const UNAUTHORIZED_USER_ERR = `You are not authorized to access this page.\n${REDIRECT_TO_SIGNIN_MSG}`;
