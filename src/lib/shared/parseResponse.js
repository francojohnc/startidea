import Parse from "parse";
import {showErrorDialog} from "components/Dialog";
import forcelyNavigateTo from "./forcelyNavigateTo";
import isUndefined from "lib/utils/isUndefined";

const handleInvalidSession = async () => {
    return showErrorDialog("We're now redirecting you to sign in page.", {
        title: "Invalid Session"
    }).then(() => {
        Parse.User.logOut();
        forcelyNavigateTo("/signin");
    });
};

export const verifyResponse = async res => {
    if (res && res.message) {
        switch (res.code) {
            case Parse.Error.INVALID_SESSION_TOKEN:
                await handleInvalidSession();
                return undefined;
            case Parse.Error.CONNECTION_FAILED:
                res.message = "Check your connection to your server";
                break;
            default:
                break;
        }

        return true;
    }

    return false;
};

export default async response => {
    const hasError = await verifyResponse(response);

    if (hasError || isUndefined(hasError)) {
        response = response.message;
    }

    return [response, hasError];
};
