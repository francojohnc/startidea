import canUseDOM from "./canUseDOM";
import isNumber from "lib/utils/isNumber";
import isArray from "lib/utils/isArray";
import isNull from "lib/utils/isNull";

export default el =>
  isNull(el) ? false : isArray(el) || (canUseDOM() && isNumber(el.length));
