import isTruthy from "lib/utils/isTruthy";
import isNil from "lib/utils/isNil";

/**
 * Props where only the prop key is used in the className.
 * @param {*} val A props value
 * @param {string} key A props key
 *
 * @example
 * <Label tag />
 * <div class="ui tag label"></div>
 */
export default (val, key, defaultVal) => {
  if (isNil(defaultVal)) {
    return isTruthy(val) && key;
  }

  return isTruthy(val) ? key : defaultVal;
};
