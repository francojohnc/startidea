import isUndefined from "lib/utils/isUndefined";

export const documentRef = {
  current: isUndefined(document) ? null : document
};

export const windowRef = {
  current: isUndefined(window) ? null : window
};

export { default as EventListener } from "./EventListener";
export { default as useEventListener } from "./useEventListener";
