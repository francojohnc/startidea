import { useEffect, useCallback, useRef } from "react";

const isActionSupported = (targetRef, method) =>
  targetRef && !!targetRef.current && !!targetRef.current[method];

const useEventListener = options => {
  const { capture, listener, type, targetRef } = options;

  const latestListener = useRef(listener);

  latestListener.current = listener;

  const eventHandler = useCallback(event => {
    return latestListener.current(event);
  }, []);

  useEffect(() => {
    const targetR = targetRef.current;

    if (isActionSupported(targetRef, "addEventListener")) {
      targetR.addEventListener(type, eventHandler, capture);
    } else if (process.env.NODE_ENV !== "production") {
      throw new Error(
        "Passed `targetRef` is not valid or does not support `addEventListener()` method."
      );
    }

    return () => {
      if (isActionSupported(targetRef, "removeEventListener")) {
        targetR.removeEventListener(type, eventHandler, capture);
      } else if (process.env.NODE_ENV !== "production") {
        throw new Error(
          "Passed `targetRef` is not valid or does not support `removeEventListener()` method."
        );
      }
    };
  }, [capture, eventHandler, targetRef, type]);
};

export default useEventListener;
