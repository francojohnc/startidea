import isEqual from "lib/utils/isEqual";

export default type => isEqual(type, "textarea");
