import isFalse from "lib/utils/isFalse";

export default (val, key) => val && isFalse(val) && `${val} ${key}`;
