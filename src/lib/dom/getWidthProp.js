import numberToWord from "lib/utils/numberToWord";

export default (val, widthClass = "", canEqual = false) => {
  if (canEqual && val === "equal") {
    return "equal width";
  }

  const valType = typeof val;
  if ((valType === "string" || valType === "number") && widthClass) {
    return `${numberToWord(val)} ${widthClass}`;
  }
  return numberToWord(val);
};
