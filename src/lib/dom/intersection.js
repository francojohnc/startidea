import isArray from "lib/utils/isArray";
import isUndefined from "lib/utils/isUndefined";

const INSTANCE_MAP = new Map();
const OBSERVER_MAP = new Map();
const ROOT_IDS = new Map();

let consecutiveRootId = 0;

function getRootId(root = null) {
  if (!root) {
    return "";
  }

  if (ROOT_IDS.has(root)) {
    return ROOT_IDS.get(root);
  }

  consecutiveRootId += 1;

  ROOT_IDS.set(root, consecutiveRootId.toString());
  return ROOT_IDS.get(root) + "_";
}

function onChange(changes = []) {
  changes.forEach(intersection => {
    const { isIntersecting, intersectionRatio, target } = intersection;
    const instance = INSTANCE_MAP.get(target);

    // Firefox can report a negative intersectionRatio when scrolling.
    /* istanbul ignore else */
    if (instance && intersectionRatio >= 0) {
      // If threshold is an array, check if any of them intersects. This just triggers the onChange event multiple times.
      let isVisible = instance.thresholds.some(threshold => {
        return instance.isVisible
          ? intersectionRatio > threshold
          : intersectionRatio >= threshold;
      });

      if (!isUndefined(isIntersecting)) {
        // If isIntersecting is defined, ensure that the element is actually intersecting.
        // Otherwise it reports a threshold of 0
        isVisible = isVisible && isIntersecting;
      }

      instance.isVisible = isVisible;
      instance.callback(isVisible, intersection);
    }
  });
}

export function observe(element, callback, options = { threshold: 0 }) {
  const { root, rootMargin, threshold } = options;

  if (!element) {
    return;
  }

  // Create a unique ID for this observer instance, based on the root, root margin and threshold.
  // An observer with the same options can be reused, so lets use this fact
  let observerId =
    getRootId(root) +
    (rootMargin
      ? `${threshold.toString()}_${rootMargin}`
      : threshold.toString());

  let observerInstance = OBSERVER_MAP.get(observerId);

  if (!observerInstance) {
    observerInstance = new IntersectionObserver(onChange, options);

    if (observerId) {
      OBSERVER_MAP.set(observerId, observerInstance);
    }
  }

  const instance = {
    callback,
    element,
    isVisible: false,
    observerId,
    observer: observerInstance,
    thresholds:
      observerInstance.thresholds ||
      (isArray(threshold) ? threshold : [threshold])
  };

  INSTANCE_MAP.set(element, instance);
  observerInstance.observe(element);

  return instance;
}

export function unobserve(element = null) {
  const instance = INSTANCE_MAP.get(element);

  if (!instance) {
    return;
  }

  const { observerId, observer } = instance;
  const { root } = observer;

  observer.unobserve(element);

  // Check if we are still observing any elements with the same threshold.
  let itemsLeft = false;
  // Check if we still have observers configured with the same root.
  let rootObserved = false;

  if (observerId) {
    INSTANCE_MAP.forEach((item, key) => {
      if (key === element) {
        return;
      }

      if (item.observerId === observerId) {
        itemsLeft = true;
        rootObserved = true;
      }

      if (item.observer.root === root) {
        rootObserved = true;
      }
    });
  }

  if (!rootObserved && root) {
    ROOT_IDS.delete(root);
  }

  if (observer && !itemsLeft) {
    // No more elements to observe for threshold, disconnect observer
    observer.disconnect();
  }

  // Remove reference to element
  INSTANCE_MAP.delete(element);
}

export function destroy() {
  OBSERVER_MAP.forEach(observer => {
    observer.disconnect();
  });

  OBSERVER_MAP.clear();
  INSTANCE_MAP.clear();
  ROOT_IDS.clear();

  consecutiveRootId = 0;
}
