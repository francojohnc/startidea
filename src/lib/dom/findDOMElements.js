import canUseDOM from "./canUseDOM";
import isRefObject from "./isRefObject";
import isFunction from "lib/utils/isFunction";
import isString from "lib/utils/isString";
import throwError from "lib/utils/throwError";

export default function(target) {
  if (isRefObject(target)) {
    return target.current;
  }

  if (isFunction(target)) {
    return target();
  }

  if (isString(target) && canUseDOM()) {
    let selection = document.querySelectorAll(target);

    if (!selection.length) {
      selection = document.querySelectorAll(`#${target}`);
    }

    if (!selection.length) {
      throwError(
        `The target '${target}' is not part of the DOM. Consider checking the spelling.`
      );
    }

    return selection;
  }

  return target;
}
