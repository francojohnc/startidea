import { Component } from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";

import handleRef from "./handleRef";
import refPropType from "./refPropType";

export default class RefFindNode extends Component {
  static displayName = "RefFindNode";

  // TODO: use Babel plugin for this
  static propTypes =
    process.env.NODE_ENV !== "production"
      ? {
          children: PropTypes.element.isRequired,
          innerRef: refPropType.isRequired
        }
      : {};

  prevNode;

  componentDidMount() {
    this.prevNode = findDOMNode(this);

    handleRef(this.props.innerRef, this.prevNode);
  }

  componentDidUpdate(prevProps) {
    const currentNode = findDOMNode(this);

    if (this.prevNode !== currentNode) {
      this.prevNode = currentNode;
      handleRef(this.props.innerRef, currentNode);
    }

    if (prevProps.innerRef !== this.props.innerRef) {
      handleRef(this.props.innerRef, currentNode);
    }
  }

  componentWillUnmount() {
    handleRef(this.props.innerRef, null);
  }

  render() {
    const { children } = this.props;

    return children;
  }
}
