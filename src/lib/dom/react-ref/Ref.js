import React, { Children } from "react";
import { isForwardRef } from "react-is";
import PropTypes from "prop-types";

import RefFindNode from "./RefFindNode";
import RefForward from "./RefForward";
import refPropType from "./refPropType";

const Ref = props => {
  const { children, innerRef } = props;

  const child = Children.only(children);
  const ElementType = isForwardRef(child) ? RefForward : RefFindNode;

  return <ElementType innerRef={innerRef}>{child}</ElementType>;
};

Ref.displayName = "Ref";

if (process.env.NODE_ENV !== "production") {
  Ref.propTypes = {
    children: PropTypes.element.isRequired,
    innerRef: refPropType.isRequired
  };
}

export default Ref;
