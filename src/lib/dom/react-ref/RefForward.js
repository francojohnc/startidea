import { Component, cloneElement } from "react";
import PropTypes from "prop-types";

import handleRef from "./handleRef";
import refPropType from "./refPropType";

export default class RefForward extends Component {
  static displayName = "RefForward";

  static propTypes =
    process.env.NODE_ENV !== "production"
      ? {
          children: PropTypes.element.isRequired,
          innerRef: refPropType.isRequired
        }
      : {};

  handleRefOverride = node => {
    const { children, innerRef } = this.props;

    handleRef(children.ref, node);
    handleRef(innerRef, node);
  };

  render() {
    const { children } = this.props;

    return cloneElement(children, {
      ref: this.handleRefOverride
    });
  }
}
