import isFunction from "lib/utils/isFunction";
import isObject from "lib/utils/isObject";

const handleRef = (ref, node) => {
  if (process.env.NODE_ENV !== "production") {
    if (typeof ref === "string") {
      throw new Error(
        "Unsupported string refs, this is a legacy API and will be likely to be removed in one of the future releases of React."
      );
    }
  }

  if (isFunction(ref)) {
    ref(node);
    return;
  }

  if (isObject(ref)) {
    // The `current` property is defined as readonly, however it's a valid way because
    // `ref` is a mutable object
    ref.current = node;
  }
};

export default handleRef;
