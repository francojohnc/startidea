import findDOMElements from "./findDOMElements";
import isArrayOrNodeList from "./isArrayOrNodeList";

export default function(target) {
  const el = findDOMElements(target);

  return isArrayOrNodeList(el) ? el[0] : el;
}
