import isTrue from "lib/utils/isTrue";

export default (val, key) => val && (isTrue(val) ? key : `${val} ${key}`);
