import isUndefined from "lib/utils/isUndefined";

export default () =>
  !!(!isUndefined(window) && window.document && window.document.createElement);
