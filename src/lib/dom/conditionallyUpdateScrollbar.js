import getScrollbarWidth from "./getScrollbarWidth";
import isBodyOverflowing from "./isBodyOverflowing";
import setBodyScrollbarWidth from "./setBodyScrollbarWidth";

export default function() {
  const scrollbarWidth = getScrollbarWidth();

  // https://github.com/twbs/bootstrap/blob/v4.0.0-alpha.6/js/src/modal.js#L433
  const fixedContent = document.querySelectorAll(
    ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top"
  )[0];

  const bodyPadding = fixedContent
    ? parseInt(fixedContent.style.paddingRight || 0, 10)
    : 0;

  if (isBodyOverflowing()) {
    setBodyScrollbarWidth(bodyPadding + scrollbarWidth);
  }
}
