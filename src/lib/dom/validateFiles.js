import isFunction from "lib/utils/isFunction";
import accepts from "lib/utils/extAccepts";
import getFilesFromEvent from "./getFilesFromEvent";
import asyncForEach from "lib/utils/asyncForEach";

const isFileMatchSize = ({ size: fileSize }, minSize, maxSize) => {
  return fileSize >= minSize && fileSize <= maxSize;
};

const isFileAccepted = (file, whiteList) => {
  return accepts(file, whiteList);
};

const getBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

const isValidMode = mode =>
  ["changeOnly", "acceptOnly", "rejectOnly"].includes(mode);

const isChangeOnly = mode => mode === "changeOnly";
const isAcceptOnly = mode => mode === "acceptOnly";
const isRejectOnly = mode => mode === "rejectOnly";

export default async (
  event,
  {
    mode = "changeOnly",
    minSize = 0,
    maxSize = Infinity,
    isMultiple = false,
    isBase64 = false,
    whiteList
  }
) => {
  event.preventDefault();

  if (isFunction(event.persist)) {
    event.persist();
  }

  if (!isValidMode(mode)) {
    throw new Error("mode must be 'changeOnly', 'acceptOnly', or 'rejectOnly'");
  }

  return Promise.resolve(getFilesFromEvent(event)).then(async files => {
    const acceptedFiles = [];
    const rejectedFiles = [];

    await asyncForEach(files, async file => {
      if (
        isFileAccepted(file, whiteList) &&
        isFileMatchSize(file, minSize, maxSize)
      ) {
        acceptedFiles.push(await (isBase64 ? getBase64(file) : file));
      } else {
        rejectedFiles.push(file);
      }
    });

    if (!isMultiple && acceptedFiles.length > 1) {
      // Reject everything and empty accepted files
      rejectedFiles.push(...acceptedFiles.splice(0));
    }

    if (isChangeOnly(mode)) {
      return Promise.resolve({ acceptedFiles, rejectedFiles, event });
    }

    if (acceptedFiles.length > 0 && isAcceptOnly(mode)) {
      return Promise.resolve({ acceptedFiles, event });
    }

    if (rejectedFiles.length > 0 && isRejectOnly(mode)) {
      return Promise.resolve({ rejectedFiles, event });
    }
  });
};
