import isArray from "lib/utils/isArray";
import isTypeOf from "lib/utils/isTypeOf";
import hasOwn from "lib/utils/hasOwn";

export default function classNames() {
  const classes = [];

  for (let i = 0; i < arguments.length; i++) {
    const arg = arguments[i];

    if (!arg) continue;

    if (isTypeOf(arg, "string") || isTypeOf(arg, "number")) {
      classes.push(arg);
    } else if (isArray(arg) && arg.length) {
      const inner = classNames.apply(null, arg);

      if (inner) {
        classes.push(inner);
      }
    } else if (isTypeOf(arg, "object")) {
      for (const key in arg) {
        if (hasOwn(arg, key) && arg[key]) {
          classes.push(key);
        }
      }
    }
  }

  return classes.join(" ");
}
