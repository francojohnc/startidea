import isLessThan from "lib/utils/isLessThan";

export default () => isLessThan(document.body.clientWidth, window.innerWidth);
