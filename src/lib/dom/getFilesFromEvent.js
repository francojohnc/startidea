export default async evt =>
  evt.target && evt.target.files ? Array.from(evt.target.files) : [];
