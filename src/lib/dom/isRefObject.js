import isTypeOf from "lib/utils/isTypeOf";

export default target =>
  target && isTypeOf(target, "object") ? "current" in target : false;
