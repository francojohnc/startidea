export default function isDetached(element) {
  if (!element) return true;

  if (
    !(element instanceof HTMLElement) ||
    element.nodeType === Node.DOCUMENT_NODE
  )
    return false;

  return isDetached(element.parentNode);
}
