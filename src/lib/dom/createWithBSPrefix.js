import React from "react";
import pascalCase from "lib/utils/pascalCase";
import cx from "./classnames";

const defaultParams = {
  displayName: "",
  Comp: "div",
  defaultProps: {}
};

export default function createWithBSPrefix(
  prefix,
  {
    displayName = pascalCase(prefix),
    Comp = "div",
    defaultProps = {}
  } = defaultParams
) {
  const BSComponent = React.forwardRef(
    ({ className, as: Tag = Comp, ...rest }, ref) => {
      return <Tag {...rest} className={cx(className, prefix)} ref={ref} />;
    }
  );

  BSComponent.displayName = displayName;
  BSComponent.defaultProps = defaultProps;

  return BSComponent;
}
