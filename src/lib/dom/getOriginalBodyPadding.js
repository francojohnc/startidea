export default function() {
  const style = window.getComputedStyle(document.body, null);

  return parseInt((style && style.getPropertyValue("padding-right")) || 0, 10);
}
