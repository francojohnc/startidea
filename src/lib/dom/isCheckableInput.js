import isCheckBoxInput from "./isCheckBoxInput";
import isRadioInput from "./isRadioInput";
import isSwitchInput from "./isSwitchInput";

export default type =>
  isCheckBoxInput(type) || isRadioInput(type) || isSwitchInput(type);
