export default function(padding) {
  document.body.style.paddingRight = padding > 0 ? `${padding}px` : null;
}
