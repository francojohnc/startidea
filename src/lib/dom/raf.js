// RequestAnimationFrame - raf

let id = 0;
const ids = {};

// Support call raf with delay specified frame
export default function wrapperRaf(callback, delayFrames = 1) {
  const myId = id++;
  let restFrames = delayFrames;

  function internalCallback() {
    restFrames -= 1;

    if (restFrames <= 0) {
      callback();
      delete ids[myId];
    } else {
      ids[myId] = window.requestAnimationFrame(internalCallback);
    }
  }

  ids[myId] = window.requestAnimationFrame(internalCallback);

  return myId;
}

wrapperRaf.cancel = function(pid) {
  if (pid === undefined) return;

  window.cancelAnimationFrame(ids[pid]);
  delete ids[pid];
};
