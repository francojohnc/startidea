import isDetached from "./isDetached";

export default function onDOMRemove(element, onDetachCallback) {
  const observer = new MutationObserver(() => {
    if (isDetached(element)) {
      observer.disconnect();
      onDetachCallback();
    }
  });

  observer.observe(window.document, {
    childList: true,
    subtree: true
  });

  return observer;
}
