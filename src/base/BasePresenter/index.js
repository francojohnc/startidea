import isUndefined from "lib/utils/isUndefined";
import parseResponse from "lib/shared/parseResponse";

export default class BasePresenter {
    constructor(view) {
        this.view = view;

        this.errorCallback = this.errorCallback.bind(this);
    }

    async errorCallback(error) {
        const [response, hasError] = await parseResponse(error);

        if (!isUndefined(hasError)) {
            await this.view.showErrorDialog("Oops...", response);
        }

        return hasError;
    }
}
