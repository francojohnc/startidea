import { Component } from "react";
import {
  showDialog,
  showErrorDialog,
  showSuccessDialog,
  showQuestionDialog
} from "components/Dialog";
import forcelyNavigateTo from "lib/shared/forcelyNavigateTo";

export default class BaseComponent extends Component {
  reloadPage() {
    window.location.reload();
  }

  showErrorSnackbar(message) {
    return showErrorDialog(message, true);
  }

  showSuccessSnackbar(message) {
    return showSuccessDialog(message, true);
  }

  showErrorDialog(title, message) {
    return showErrorDialog(message, { title });
  }

  showSuccessDialog(title, message) {
    return showSuccessDialog(message, { title });
  }

  showQuestionDialog(message, title = "Are you sure?", options = {}) {
    return showQuestionDialog(message, {
      title,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      ...options
    });
  }

  showDialog(options) {
    return showDialog(options);
  }

  async showConfirmationDialog(message = "You won't be able to revert this.") {
    return this.showQuestionDialog(message);
  }

  navigateToSigninPage(isForce = true) {
    this.navigateTo("/signin", isForce);
  }

  navigateToHomePage(isForce = true) {
    this.navigateTo("/", isForce);
  }

  navigateTo(link, isForce = false) {
    isForce ? forcelyNavigateTo(link) : this.props.history.push(link);
  }

  scrollToTop = () => {
    window.requestAnimationFrame(() =>
      window.scrollTo({ top: 0, behavior: "smooth" })
    );
  };
}
