import BaseComponent from "./";

export default class BaseComponentContext extends BaseComponent {
    setCurrentUser(currentUser) {
        this.props.setCurrentUser(currentUser);
    }

    getCurrentUser() {
        return this.props.currentUser;
    }

}
