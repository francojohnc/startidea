import { createContext } from "react";

const MainPageContext = createContext({});

export const MainPageProvider = MainPageContext.Provider;

export default MainPageContext;
