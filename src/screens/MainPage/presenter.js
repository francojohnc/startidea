import GetCurrentUserUseCase from "domain/user/GetCurrentUserUseCase";

class Presenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.getCurrentUserSubscription = this.runGetCurrentUser();
    }

    componentWillUnmount() {
        this.getCurrentUserSubscription &&
        this.getCurrentUserSubscription.unsubscribe();
    }

    runGetCurrentUser() {
        return GetCurrentUserUseCase().subscribe({
            next: (user) => {
                this.view.setCurrentUser(user);
            },
            error: () => {
                this.view.navigateToSigninPage();
            }
        });
    }

}

export default Presenter;
