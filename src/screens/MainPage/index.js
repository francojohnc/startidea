import React from "react";
import BaseComponentContext from "base/BaseComponent/BaseComponentContext";
import {Route, Switch} from "react-router-dom";
import Layout from "components/Layout";
import {MainPageProvider} from "./context";
import Presenter from "./presenter";
import CreatePage from "screens/CreatePage";
import GeneratePage from "../GeneratePage";
import "./style.css";
import Sidebar from "./Sidebar";
import Toolbar from "./Toolbar";
import Spinner from "components/Spinner";
import {withContext} from "AppContext";


class MainPage extends BaseComponentContext {

    constructor(props) {
        super(props);
        this.presenter = new Presenter(this);
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    componentWillUnmount() {
        this.presenter.componentWillUnmount();
    }

    render() {
        let {currentUser} = this.props;
        if (!currentUser) {
            return (
                <div className="global-loader">
                    <Spinner size="extra-large"/>
                </div>
            );
        }
        return (
            <MainPageProvider>
                <Layout>
                    <Sidebar/>
                    <Layout>
                        <Toolbar/>
                        <Switch>
                            <Route path="/create" component={CreatePage}/>
                            <Route path="/generate" component={GeneratePage}/>
                        </Switch>
                    </Layout>
                </Layout>
            </MainPageProvider>
        );
    }
}

export default withContext(MainPage);
