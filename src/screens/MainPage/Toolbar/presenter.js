import SignOutUser from "domain/user/SignOutUser";

class Presenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.signOutUserSubscription && this.signOutUserSubscription.unsubscribe();
    }

    logOutClick = async () => {
        const {value} = await this.view.showConfirmationDialog("you want to logout?");
        if (!value) {
            return;
        }
        this.signOutUserSubscription = SignOutUser().subscribe({
            next: () => {
                this.view.navigateToSigninPage();
            },
            error: () => {
                this.view.navigateToSigninPage();
            }
        });
    }

}

export default Presenter;
