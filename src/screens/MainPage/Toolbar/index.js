import React from "react";
import dp from "./dp.jpg";
import Layout from "components/Layout";
import SidebarToggler from "custom/components/SidebarToggler";
import Dropdown from "components/Dropdown";
import Icon from "components/Icon";
import {withContext} from "AppContext";
import Presenter from "./presenter";
import BaseComponentContext from "base/BaseComponent/BaseComponentContext";

const {Header} = Layout;


class Toolbar extends BaseComponentContext {

    constructor(props) {
        super(props);
        this.presenter = new Presenter(this);
    }

    render() {
        let {currentUser} = this.props;
        return (
            <Header className="bg-white main-header px-3 d-flex justify-content-md-between">
                <SidebarToggler/>
                <Dropdown className="d-flex justify-content-end">
                    <Dropdown.Toggle as="div">
                        <div class="float-left pr-3" style={{width: "20%"}}>
                            <img
                                src={dp}
                                class="rounded-circle"
                                style={{
                                    width: "35px",
                                    background: "35px",
                                    "object-fit": "cover"
                                }}
                            />
                        </div>
                        <div
                            class="float-left pl-3 font-md"
                            style={{width: "80%", "padding-left": "3px"}}>
                            {currentUser.get("firstName") + " " + currentUser.get("lastName")}
                            <Icon name="angle-down" className="pl-2"/>
                        </div>
                        <div class="clearfix"/>
                    </Dropdown.Toggle>
                    <Dropdown.Menu right>
                        <Dropdown.Item style={{"line-height": "0"}}>
                            <Icon name="user" className="pr-2"/>
                            Account Profile
                        </Dropdown.Item>
                        <div class="dropdown-divider"/>
                        <Dropdown.Item style={{"line-height": "0"}}
                                       onClick={this.presenter.logOutClick}>
                            <Icon name="sign-out" className="pr-2"/>
                            Logout
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Header>
        );
    }
}

export default withContext(Toolbar);
