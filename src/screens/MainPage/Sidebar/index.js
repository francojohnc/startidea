import React from "react";
import logo from "./logo.svg";
import {Link} from "react-router-dom";
import Button from "components/Button";
import Menu from "components/Menu";
import Layout from "components/Layout";

const {Sidebar} = Layout;

class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSidebarCollapsed: true,
        };
    }

    onSidebarCollapse = (isSidebarCollapsed) => {
        this.setState({isSidebarCollapsed});
    }

    render() {
        const {isSidebarCollapsed} = this.state;
        return (
            <Sidebar
                width="300px"
                className="sidebar--fixed"
                onCollapse={this.onSidebarCollapse}
                isCollapsed={isSidebarCollapsed}
                isPushable>
                <Button
                    variant="link"
                    className="navbar-brand w-100 d-flex justify-content-center
                            flex-wrap align-items-center signin-container"
                    style={{height: "64px"}} >
                    <img
                        src={logo}
                        style={{width: "180px"}}
                        className="img-fluid"
                    />
                </Button>
                <div className="profile-display p-3">
                    <Button
                        as={Link}
                        to="/create"
                        content="CREATE"
                        className="rounded"
                        isBlock
                        style={{"font-size": "12px"}}
                    />
                </div>
                <Menu size="medium" className="mt-2" icon isVertical>
                    <Menu.Item
                        name="cash-register"
                        className="px-4 py-3"
                        icon="calculator"
                        content="My Ideas"
                        active={this.state.activeItem === "cash-register"}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        name="transactions"
                        className="px-4 py-3"
                        icon="exchange"
                        content="Transactions"
                        active={this.state.activeItem === "transactions"}
                        onClick={this.handleItemClick}
                    />
                </Menu>
            </Sidebar>
        );
    }
}

export default SideBar;
