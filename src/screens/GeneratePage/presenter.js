import GenerateIdeaUseCase from "domain/idea/GenerateIdeaUseCase";

class Presenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        let idea = this.view.getIdea();
        this.generateSubscription = this.generate(idea);
    }

    componentWillUnmount() {
        this.generateSubscription && this.generateSubscription.unsubscribe();
    }

    generate(idea) {
        return GenerateIdeaUseCase(idea.id).subscribe({
            next: (res) => {
                console.log(res);
            },
            error: (err) => {
                console.log(err);
            }
        });
    }

}

export default Presenter
