import React from "react";
import BaseContextComponent from "components/BaseComponent/BaseContextComponent";
import Presenter from "./presenter";
import Layout from "components/Layout";
import bgrequest from "./bg-requested.svg";

import "./style.css";

class GeneratePage extends BaseContextComponent {
    constructor(props) {
        super(props);

        this.presenter = new Presenter(this);
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    componentWillUnmount() {
        this.presenter.componentWillUnmount();
    }

    getIdea() {
        return this.props.history.location.state;
    }


    render() {
        return (
            <Layout>
                <div class="bg-muted py-5">
                    <div class="title-block mb-4 px-2">
                        <h2 class="text-primary">Building you Idea</h2>
                    </div>
                    <div className="col-md-9 px-2">
                        <div className="row mx-0">
                            <div className="col-md-2 offset-md-5 text-center">
                                <div class="position-relative">
                                    <div class="spinner">
                                        <div class="double-bounce1"/>
                                        <div class="double-bounce2"/>
                                    </div>
                                    <img src={bgrequest} class="img-fluid w-100"/>
                                    <p class="saving mt-5">
                                        Creating sub-domain&nbsp;
                                        <label className="text-primary">
                                            fuego-shop.startidea.co
                                        </label>
                                        <span>.</span>
                                        <span>.</span>
                                        <span>.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default GeneratePage;
