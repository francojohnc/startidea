import BasePresenter from "base/BasePresenter";
import SignInUser from "domain/user/SignInUser";

export default class Presenter extends BasePresenter {
    constructor(view) {
        super(view);
        this.view = view;
    }

    componentWillUnmount() {
        this.signinSubscription && this.signinSubscription.unsubscribe();
    }

    submitForm({username, password}) {
        this.view.setSubmitting(true);
        this.signinSubscription = SignInUser(
            username,
            password
        ).subscribe({
            next: (user) => {
                this.view.setSubmitting(false)
                this.view.setCurrentUser(user);
                this.view.navigateToHomePage();
            },
            error: (err) => {
                this.view.setSubmitting(false);
                this.errorCallback(err);
            }
        });
    }


}
