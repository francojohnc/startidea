import React from "react";
import {Link} from "react-router-dom";
import BaseComponentContext from "base/BaseComponent/BaseComponentContext";
import Button from "components/Button";
import Input from "components/Input";
import Form from "components/Form";
import withForm from "lib/hoc/withForm";
import AutContainer from "screens/auth/AuthContainer";
import Presenter from "./presenter";
import icon from "./icnonic-bg.svg";
import {withContext} from "AppContext";

class Index extends BaseComponentContext {

    constructor(props) {
        super(props);

        this.presenter = new Presenter(this);

        this.state = {
            isSubmitting: false
        };

        this.submitForm = this.submitForm.bind(this);
        this.setFormState = this.setFormState.bind(this);
        this.setSubmitting = this.setSubmitting.bind(this);
    }

    componentWillUnmount() {
        this.presenter.componentWillUnmount();
    }

    setFormState(formState) {
        this.setState(formState);
    }

    setSubmitting(isSubmitting) {
        this.setState({isSubmitting});
    }

    submitForm(data) {
        this.presenter.submitForm(data);
    }

    render() {
        const {handleSubmit, register} = this.props;
        const {isSubmitting} = this.state;

        return (
            <AutContainer>
                <div className="signin-container row mx-0 h-100">
                    <div className="col-md-4 px-0 position-relative overflow-hidden d-none d-md-block">
                        <div
                            className="signin-bg h-100 d-flex justify-content-center flex-wrap align-items-center signin-container">
                            <div
                                className="w-100 h-100 position-absolute"
                                style={{background: "rgba(0,0,0,.30)"}}
                            />
                            <img
                                src={icon}
                                className="position-absolute"
                                style={{
                                    height: "105%",
                                    left: "-240px",
                                    top: "0",
                                    opacity: "0.2"
                                }}
                            />
                            <div className="text-center text-white position-relative">
                                <h1 className="mb-4">Hello, Friend</h1>
                                <p className="mb-4">
                                    Enter your personal details and start your
                                    <br/> journey with us.
                                </p>
                                <Button
                                    type="submit"
                                    as={Link}
                                    to={"/signup"}
                                    className="signin-btn d-inline-block w-auto border-0 rounded signin-btn--elevate"
                                    isBlock
                                >
                                    SIGN UP
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div
                        className="col-md-8 px-0 h-100 d-flex justify-content-center flex-wrap align-items-center signin-container">
                        <div className="signin">
                            <h3 className="signin-header">
                                <h1>Sign In to</h1> StartIdea
                            </h3>
                            <Form
                                onSubmit={handleSubmit(this.submitForm)}
                                isLoading={isSubmitting}>
                                <Input
                                    name="username"
                                    type="text"
                                    placeholder="username"
                                    className="signin__input text-center"
                                    icon="user"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <Input
                                    name="password"
                                    type="password"
                                    placeholder="password"
                                    className="signin__input text-center"
                                    icon="lock"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <div className="d-flex justify-content-end mb-4">
                                    <Button className="signin-link p-0 mt-2" variant="link">
                                        Forgot password?
                                    </Button>
                                </div>
                                <div className="px-5 text-center">
                                    <Button
                                        type="submit"
                                        className="signin-btn signin-btn--elevate rounded"
                                        isBlock
                                    >
                                        Sign In
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </AutContainer>
        );
    }
}

export default withContext(withForm({useSubmissionState: false})(Index));
