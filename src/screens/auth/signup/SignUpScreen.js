import React from "react";
import {Link} from "react-router-dom";
import BaseComponentContext from "base/BaseComponent/BaseComponentContext";
import Button from "components/Button";
import Input from "components/Input";
import Form from "components/Form";
import withForm from "lib/hoc/withForm";
import AutContainer from "screens/auth/AuthContainer";
import SignUpPresenter from "./SignUpPresenter";
import icon from "./icnonic-bg.svg";
import {withContext} from "../../../AppContext";

class SignUpScreen extends BaseComponentContext {

    constructor(props) {
        super(props);

        this.presenter = new SignUpPresenter(this);

        this.state = {
            isSubmitting: false
        };

        this.submitForm = this.submitForm.bind(this);
        this.setFormState = this.setFormState.bind(this);
        this.setSubmitting = this.setSubmitting.bind(this);
    }

    componentWillUnmount() {
        this.presenter.componentWillUnmount();
    }


    setFormState(formState) {
        this.setState(formState);
    }

    setSubmitting(isSubmitting) {
        this.setState({isSubmitting});
    }

    submitForm(data) {
        this.presenter.submitForm(data);
    }

    render() {
        const {handleSubmit, register} = this.props;
        const {isSubmitting} = this.state;

        return (
            <AutContainer>
                <div className="signin-container row mx-0 h-100">
                    <div className="col-md-4 px-0 position-relative overflow-hidden d-none d-md-block">
                        <div
                            className="signin-bg h-100 d-flex justify-content-center flex-wrap align-items-center signin-container">
                            <div
                                className="w-100 h-100 position-absolute"
                                style={{background: "rgba(0,0,0,.30)"}}
                            />
                            <img
                                src={icon}
                                className="position-absolute"
                                style={{
                                    height: "105%",
                                    left: "-240px",
                                    top: "0",
                                    opacity: "0.2"
                                }}
                            />
                            <div className="text-center text-white position-relative">
                                <h1 className="mb-4">Welcome Back!</h1>
                                <p className="mb-4">
                                    To keep connected with us please login with your
                                    <br/> personal info.
                                </p>
                                <Button
                                    type="submit"
                                    className="signin-btn d-inline-block w-auto border-0 rounded signin-btn--elevate"
                                    isBlock
                                    as={Link}
                                    to={"/signin"}
                                >
                                    SIGN IN
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div
                        className="col-md-8 px-0 h-100 d-flex justify-content-center flex-wrap align-items-center signin-container">
                        <div className="signin">
                            <h3 className="signin-header">
                                <h1>Sign Up to</h1> StartIdea
                            </h3>
                            <Form
                                onSubmit={handleSubmit(this.submitForm)}
                                isLoading={isSubmitting}
                            >
                                <Input
                                    required
                                    autoComplete={false}
                                    name="firstName"
                                    type="text"
                                    placeholder="first name"
                                    className="signin__input text-center"
                                    icon="user"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <Input
                                    required
                                    autoComplete={false}
                                    name="lastName"
                                    type="text"
                                    placeholder="last name"
                                    className="signin__input text-center"
                                    icon="user"
                                    iconType="lineAwesome"
                                    ref={register}
                                />

                                <Input
                                    required
                                    autoComplete={false}
                                    name="email"
                                    type="email"
                                    placeholder="email"
                                    className="signin__input text-center"
                                    icon="envelope"
                                    iconType="lineAwesome"
                                    ref={register}
                                />

                                <Input
                                    required
                                    autoComplete={false}
                                    name="mobileNumber"
                                    type="number"
                                    placeholder="mobile number"
                                    className="signin__input text-center"
                                    icon="mobile"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <Input
                                    required
                                    autoComplete={false}
                                    name="birthday"
                                    type="date"
                                    placeholder="birthday"
                                    className="signin__input text-center"
                                    icon="calendar"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <Input
                                    required
                                    autoComplete={false}
                                    name="password"
                                    type="password"
                                    placeholder="password"
                                    className="signin__input text-center"
                                    icon="lock"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <Input
                                    required
                                    autoComplete={false}
                                    name="rePassword"
                                    type="password"
                                    placeholder="re password"
                                    className="signin__input text-center"
                                    icon="lock"
                                    iconType="lineAwesome"
                                    ref={register}
                                />
                                <div className="px-5 mt-5">
                                    <Button
                                        type="submit"
                                        className="signin-btn signin-btn--elevate"
                                        isBlock
                                    >
                                        Submit
                                    </Button>
                                </div>
                                <div className="text-center mt-5">
                                    <p className="d-inline-block d-md-none">
                                        Have account already?
                                        <Button className="signin-link p-0" variant="link">
                                            Sign In
                                        </Button>
                                    </p>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </AutContainer>
        );
    }
}

export default withContext(withForm({useSubmissionState: false})(SignUpScreen));
