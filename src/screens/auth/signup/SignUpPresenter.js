import isUndefined from "lib/utils/isUndefined";
import BasePresenter from "lib/shared/BasePresenter";
import SignUpUseCase from "domain/user/SignUpUseCase";


export default class SignUpPresenter extends BasePresenter {
    constructor(view) {
        super(view);

        this.view = view;

        this.submitFormSuccess = this.submitFormSuccess.bind(this);
        this.submitFormError = this.submitFormError.bind(this);
    }

    componentWillUnmount() {
        this.signUpSubscription && this.signUpSubscription.unsubscribe();
    }

    submitForm({password, rePassword, email, firstName, lastName, birthday, mobileNumber}) {
        if (password !== rePassword) {
            this.view.showErrorDialog("Invalid", "password most be the same");
            return;
        }
        birthday = new Date(birthday);
        this.view.setSubmitting(true);
        this.signUpSubscription = SignUpUseCase({
            email,
            password,
            firstName,
            lastName,
            birthday,
            mobileNumber
        }).subscribe({
            next: this.submitFormSuccess,
            error: this.submitFormError
        });
    }

    submitFormSuccess(user) {
        this.view.setSubmitting(false);
        this.view.setCurrentUser(user);
        this.view.navigateToHomePage();
    }

    async submitFormError(error) {
        if (!isUndefined(await this.errorCallback(error))) {
            this.view.setSubmitting(false);
        }
    }
}
