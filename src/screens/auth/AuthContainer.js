import React from "react";
import "./style.min.css";
import logoW from "./logo-w.svg";
import logo from "./logo.svg";

export default function AutContainer({ children }) {
  return (
    <div className="h-100 w-100">
      <img
        className="position-fixed d-none d-md-block"
        src={logoW}
        style={{ top: "24px", left: "24px", zIndex: "999", width: "180px" }}
      />
      <img
        className="position-fixed d-block d-md-none"
        src={logo}
        style={{ top: "24px", left: "24px", zIndex: "999", width: "180px" }}
      />
      {children}
      {/*d-flex justify-content-center flex-wrap align-items-center signin-container*/}
    </div>
  );
}
