import React from 'react';
import BaseContextComponent from "components/BaseComponent/BaseContextComponent";
import SplashPresenter from "./SplashScreenPresenter";


class SplashScreen extends BaseContextComponent {
    presenter = new SplashPresenter(this);

    render() {
        return (
            <div className="App">
                <h1>hello</h1>
            </div>
        );
    }

}

export default SplashScreen;
