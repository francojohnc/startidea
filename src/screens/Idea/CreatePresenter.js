import GetProductsUseCase from "domain/product/GetProductsUseCase";
import SaveIdeaUseCase from "domain/idea/SaveIdeaUseCase";

class CreatePresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.productObserver = this.getProducts();
    }

    componentWillUnmount() {
        this.productObserver && this.productObserver.unsubscribe();
        this.saveIdeaObserver && this.saveIdeaObserver.unsubscribe();
    }

    submitForm(data) {
        this.saveIdeaObserver = SaveIdeaUseCase(data).subscribe({
            next: () => {
                console.log("");
            },
            error: (err) => {
                console.log(err);
            }
        });
    }

    getProducts() {
        return GetProductsUseCase().subscribe({
            next: (products) => {
                this.view.setProducts(products);
            },
            error: (err) => {
                console.log(err);
            }
        });
    }

}

export default CreatePresenter
