import React from "react";
import BaseContextComponent from "components/BaseComponent/BaseContextComponent";
import CreatePresenter from "./CreatePresenter";
import Layout from "components/Layout";
import Button from "components/Button";
import Input from "components/Input";
import Form from "components/Form";
import withForm from "lib/hoc/withForm";
import bgrequest from "./bg-requested.svg";
import patxt from "./patxt.svg";

import "./style.min.css";
import Dropdown from "components/Dropdown";

class Index extends BaseContextComponent {
  constructor(props) {
    super();

    this.presenter = new CreatePresenter(this);

    this.state = {
      isSubmitting: false,
      products: []
    };

    this.submitForm = this.submitForm.bind(this);
    this.setFormState = this.setFormState.bind(this);
    this.setSubmitting = this.setSubmitting.bind(this);
    this.setProducts = this.setProducts.bind(this);
  }

  componentDidMount() {
    this.presenter.componentDidMount();
  }

  componentWillUnmount() {
    this.presenter.componentWillUnmount();
  }

  getAppContext() {
    return this.context;
  }

  setProducts(products) {
    this.setState({ products });
  }

  setFormState(formState) {
    this.setState(formState);
  }

  setSubmitting(isSubmitting) {
    this.setState({ isSubmitting });
  }

  submitForm(data) {
    this.presenter.submitForm(data);
  }

  render() {
    const { handleSubmit, register } = this.props;
    const { isSubmitting } = this.state;
    const { products } = this.state;

    return (
      <Layout>
        <div className="row mx-0">
          <div className="col-md-12 px-2">
            <div class="admin-content p-4">
              <div class="title-block mb-4 px-2">
                <h2 class="text-primary">My Idea</h2>
              </div>
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-3 px-2">
                  <div class="card shadow mb-3">
                    <div class="card-body">
                      <div class="row mx-0">
                        <div class="col-3 px-2">
                          <img src={patxt} class="img-fluid" />
                        </div>
                        <div class="col-9 px-2 m-auto">
                          <h5 class="text-primary mb-1">Patxt sms Gateway</h5>
                          <p class="font-sm mb-0">
                            Far far away, behind the word mountains, far from
                            the countries, there live the text.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 px-2">
                  <div class="card shadow mb-3">
                    <div class="card-body">
                      <div class="row mx-0">
                        <div class="col-3 px-2">
                          <img src={patxt} class="img-fluid" />
                        </div>
                        <div class="col-9 px-2 m-auto">
                          <h5 class="text-primary mb-1">Patxt sms Gateway</h5>
                          <p class="font-sm mb-0">
                            Far far away, behind the word mountains, far from
                            the countries, there live the text.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 px-2">
                  <div class="card shadow mb-3">
                    <div class="card-body">
                      <div class="row mx-0">
                        <div class="col-3 px-2">
                          <img src={patxt} class="img-fluid" />
                        </div>
                        <div class="col-9 px-2 m-auto">
                          <h5 class="text-primary mb-1">Patxt sms Gateway</h5>
                          <p class="font-sm mb-0">
                            Far far away, behind the word mountains, far from
                            the countries, there live the text.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 px-2">
                  <div class="card shadow mb-3">
                    <div class="card-body">
                      <div class="row mx-0">
                        <div class="col-3 px-2">
                          <img src={patxt} class="img-fluid" />
                        </div>
                        <div class="col-9 px-2 m-auto">
                          <h5 class="text-primary mb-1">Patxt sms Gateway</h5>
                          <p class="font-sm mb-0">
                            Far far away, behind the word mountains, far from
                            the countries, there live the text.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mx-0">
          <div className="col-md-9 px-2">
            <Form
              onSubmit={handleSubmit(this.submitForm)}
              isLoading={isSubmitting}
              className="p-4"
            >
              <div class="title-block mb-4 px-2">
                <h2 class="text-primary">Create Idea</h2>
              </div>
              <Form.Label className="mt-3 d-block">CREATE IDEA</Form.Label>
              <Input
                name="name"
                type="text"
                className="signin__input text-center"
                icon="pencil"
                iconType="lineAwesome"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">CHOOSE IDEA</Form.Label>
              <select className="form-control">
                <option value="TNVS">TNVS</option>
                <option value="POS">POS</option>
                <option value="ECOMMERCE">E-ECOMMERCE</option>
                <option value="INVENTORY">INVENTORY</option>
                <option value="OTHERS">OTHERS</option>
              </select>
              <Form.Label className="mt-3 d-block">
                SUB DOMAIN NAME
                <span className="text-muted"> (www.myidea.startidea.co)</span>
              </Form.Label>
              <Input
                name="description"
                type="text"
                className="signin__input text-center"
                icon="globe"
                iconType="lineAwesome"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">DESCRIPTION</Form.Label>
              <textarea
                name="description"
                className="signin__input w-100 form-control"
                ref={register}
              />
              <Button
                type="submit"
                className="signin-btn signin-btn--elevate mt-3 rounded"
              >
                NEXT
              </Button>
            </Form>
          </div>
        </div>
        <hr />
        <Form
          onSubmit={handleSubmit(this.submitForm)}
          isLoading={isSubmitting}
          className="p-4"
        >
          <div className="row mx-0">
            <div className="col-md-9 px-2">
              <div class="title-block mb-4 px-2">
                <h2 class="text-primary">Others</h2>
              </div>
              <Form.Label className="mt-3 d-block">PROJECT NAME</Form.Label>
              <Input
                name="name"
                type="text"
                className="signin__input text-center"
                icon="pencil"
                iconType="lineAwesome"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">TARGET DATE</Form.Label>
              <Input
                name="name"
                type="date"
                className="signin__input text-center"
                icon="calendar"
                iconType="lineAwesome"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">ESTIMATED BUDGET</Form.Label>
              <Input
                name="name"
                type="number"
                className="signin__input text-center"
                icon="dollar"
                iconType="lineAwesome"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">CHOOSE IDEA</Form.Label>
              <select className="form-control" selected>
                <option value="OTHERS">OTHERS</option>
                <option value="TNVS">TNVS</option>
                <option value="POS">POS</option>
                <option value="ECOMMERCE">E-ECOMMERCE</option>
                <option value="INVENTORY">INVENTORY</option>
              </select>
              <Form.Label className="mt-3 d-block">REQUIREMENTS</Form.Label>
              <textarea
                name="REQUIREMENTS"
                className="signin__input w-100 form-control"
                ref={register}
              />
              <Form.Label className="mt-3 d-block">
                Upload Details <label className="text-muted">(optional)</label>
              </Form.Label>
              <div class="form-group">
                <input
                  type="file"
                  class="form-control-file"
                  id="exampleFormControlFile1"
                />
              </div>
              <Button
                type="submit"
                className="signin-btn signin-btn--elevate mt-3 rounded"
              >
                NEXT
              </Button>
            </div>
          </div>
        </Form>

        <div class="bg-muted py-5">
          <div class="title-block mb-4 px-2">
            <h2 class="text-primary">Building you Idea</h2>
          </div>
          <div className="col-md-9 px-2">
            <div className="row mx-0">
              <div className="col-md-2 offset-md-5 text-center">
                <div class="position-relative">
                  <div class="spinner">
                    <div class="double-bounce1" />
                    <div class="double-bounce2" />
                  </div>
                  <img src={bgrequest} class="img-fluid w-100" />
                  <p class="saving mt-5">
                    Creating sub-domain&nbsp;
                    <label className="text-primary">
                      fuego-shop.startidea.co
                    </label>
                    <span>.</span>
                    <span>.</span>
                    <span>.</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default withForm({ useSubmissionState: false })(Index);
