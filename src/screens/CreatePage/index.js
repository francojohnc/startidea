import React from "react";
import BaseContextComponent from "components/BaseComponent/BaseContextComponent";
import Presenter from "./presenter";
import Layout from "components/Layout";
import Button from "components/Button";
import Input from "components/Input";
import Form from "components/Form";
import withForm from "lib/hoc/withForm";


class CreatePage extends BaseContextComponent {
    constructor(props) {
        super();

        this.presenter = new Presenter(this);

        this.state = {
            isSubmitting: false,
            products: []
        };
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    componentWillUnmount() {
        this.presenter.componentWillUnmount();
    }

    setProducts = (products) => {
        this.setState({products});
    }

    setSubmitting(isSubmitting) {
        this.setState({isSubmitting});
    }

    navigateToGenerate(idea) {
        this.props.history.push(`/generate`, idea);
    }

    submitForm = (data) => {
        this.presenter.submitForm(data);
    }

    render() {
        const {handleSubmit, register} = this.props;
        const {isSubmitting} = this.state;
        const {products} = this.state;

        return (
            <Layout>
                <div className="row mx-0">
                    <div className="col-md-9 px-2">
                        <Form
                            onSubmit={handleSubmit(this.submitForm)}
                            isLoading={isSubmitting}
                            className="p-4">
                            <div class="title-block mb-4 px-2">
                                <h2 class="text-primary">Create Idea</h2>
                            </div>
                            <Form.Label className="mt-3 d-block">CHOOSE IDEA</Form.Label>

                            <select name="productId" ref={register} required className="form-control">
                                <option selected disabled hidden>Choose here</option>
                                {
                                    products.map(p => {
                                        return (
                                            <option value={p.id}>{p.get("name")}</option>
                                        )
                                    })
                                }
                                <option value="OTHERS">OTHERS</option>
                            </select>
                            <Form.Label className="mt-3 d-block">
                                Name
                                <span className="text-muted"> (name of your idea or company name)</span></Form.Label>
                            <Input
                                required
                                name="name"
                                type="text"
                                className="signin__input text-center"
                                icon="pencil"
                                iconType="lineAwesome"
                                ref={register}
                            />
                            <Form.Label className="mt-3 d-block">
                                SUB DOMAIN
                                <span className="text-muted"> (www.myidea.startidea.co)</span>
                            </Form.Label>
                            <Input
                                required
                                name="domain"
                                type="text"
                                className="signin__input text-center"
                                icon="globe"
                                iconType="lineAwesome"
                                ref={register}
                            />
                            <Form.Label className="mt-3 d-block">
                                DESCRIPTION
                                <span className="text-muted"> (optional)</span>
                            </Form.Label>
                            <textarea
                                name="description"
                                className="signin__input w-100 form-control"
                                ref={register}
                            />
                            <Button
                                type="submit"
                                className="signin-btn signin-btn--elevate mt-3 rounded">
                                NEXT
                            </Button>
                        </Form>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default withForm({useSubmissionState: false})(CreatePage);
