import BasePresenter from "base/BasePresenter";
import GetProductsUseCase from "domain/product/GetProductsUseCase";
import SaveIdeaUseCase from "domain/idea/SaveIdeaUseCase";

class Presenter extends BasePresenter {
    constructor(view) {
        super(view);
        this.view = view;
    }

    componentDidMount() {
        this.productObserver = this.getProducts();
    }

    componentWillUnmount() {
        this.productObserver && this.productObserver.unsubscribe();
        this.saveIdeaObserver && this.saveIdeaObserver.unsubscribe();
    }

    submitForm(data) {
        this.view.setSubmitting(true);
        this.saveIdeaObserver = SaveIdeaUseCase(data).subscribe({
            next: (idea) => {
                this.view.setSubmitting(false);
                this.view.navigateToGenerate(idea);
            },
            error: (err) => {
                this.view.setSubmitting(false);
                this.errorCallback(err);
            }
        });
    }

    getProducts() {
        return GetProductsUseCase().subscribe({
            next: (products) => {
                this.view.setProducts(products);
            },
            error: (err) => {
                this.errorCallback(err);
            }
        });
    }

}

export default Presenter
