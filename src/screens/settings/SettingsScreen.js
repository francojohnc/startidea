import React, {Component} from "react";
import Layout from "components/Layout";
import SidebarToggler from "custom/components/SidebarToggler";
import {Tabs, Tab, TabContent} from "components/Tabs";
import General from "./scenes/General";
import "./styles.min.css";

const {Header, Content} = Layout;

class SettingsScreen extends Component {
    render() {
        return (
            <Layout className="bg-white settings">
                <Header className="bg-white main-header px-3">
                    <SidebarToggler/>
                </Header>
                <Content>
                    <Tabs>
                        <Tab className="active">Receipt</Tab>
                    </Tabs>
                    <TabContent>
                        <General/>
                    </TabContent>
                </Content>
            </Layout>
        );
    }
}

export default SettingsScreen;
