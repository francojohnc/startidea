import React, { Component } from "react";
import Form from "components/Form";
import ImagePicker from "custom/components/ImagePicker";
import Input from "components/Input";

export default class General extends Component {
  constructor(props) {
    super(props);

    this.state = { logo: "" };

    this.handleImageChange = this.handleImageChange.bind(this);
  }

  handleImageChange(logo) {
    this.setState({ logo });
  }

  render() {
    const { logo } = this.state;

    return (
      <div>
        <Form>
          <div className="mb-4">
            <label className="item-label">Logo</label>
            <ImagePicker src={logo} onChange={this.handleImageChange} base64 />
          </div>
          <Form.Row>
            <Form.Group className="col-md-6 mb-4">
              <Form.Label className="item-label">Full Name</Form.Label>
              <Input />
            </Form.Group>
            <Form.Group className="col-md-6 mb-4">
              <Form.Label className="item-label">Company</Form.Label>
              <Input />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group className="col-md-6 mb-4">
              <Form.Label className="item-label">Website</Form.Label>
              <Input />
            </Form.Group>
            <Form.Group className="col-md-6 mb-4">
              <Form.Label className="item-label">Email</Form.Label>
              <Input />
            </Form.Group>
          </Form.Row>
        </Form>
      </div>
    );
  }
}
