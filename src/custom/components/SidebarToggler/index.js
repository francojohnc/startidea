import React, {useContext, memo} from "react";
import Icon from "components/Icon";

const SidebarToggler = memo(() => {

    return (
        <div className="d-flex">
            <div className="main-header__bars">
                <Icon name="bars"/>
            </div>
            <span className="main-header__title"></span>
        </div>
    );
});

SidebarToggler.displayName = "SidebarToggle";

export default SidebarToggler;
