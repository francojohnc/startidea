import React, { Component } from "react";
import Business from "./Business";
import { withContext } from "AppContext";
import "./styles.min.css";

class ReceiptToPrint extends Component {
  render() {
    const { currentStore, transaction } = this.props;

    return (
      <div>
        <Business transaction={transaction} store={currentStore} />
      </div>
    );
  }
}

export default withContext(ReceiptToPrint);
