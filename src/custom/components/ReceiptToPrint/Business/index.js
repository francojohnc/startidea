import React, { Fragment } from "react";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";
import "./styles.min.css";

export default function Business(props) {
  return (
    <Fragment>
      <div className="invoice--business">
        <Header {...props} />
        <Main {...props} />
        <Footer {...props} />
      </div>
    </Fragment>
  );
}
