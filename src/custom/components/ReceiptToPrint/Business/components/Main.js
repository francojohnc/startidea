import React, { Fragment } from "react";
import formatCurrency from "lib/utils/formatCurrency";
import formatDate from "lib/utils/formatDate";
import isEmpty from "lib/utils/isEmpty";

export default function Main({ transaction }) {
  const payments = transaction.get("payments");
  const paymentMode = transaction.get("paymentMode").get("name");
  const items = transaction.get("items");
  const notes = transaction.get("notes");
  const subTotalPrice = transaction.get("subTotalPrice");
  const totalPrice = transaction.get("totalPrice");
  const discountType = transaction.get("discountType");
  const discount = formatCurrency(
    discountType === "FIXED"
      ? transaction.get("discount")
      : subTotalPrice - totalPrice
  );

  const hasNoPaymentMade = isEmpty(payments);
  const isSinglePayment = paymentMode.toLowerCase() === "one-time payment";

  const payment = payments[0];
  const paymentMethod = payment ? payment.get("method").get("name") : "";
  const totalPriceFormatted = formatCurrency(totalPrice);
  const tenderedAmount = formatCurrency(
    payment ? payment.get("tenderedAmount") : 0
  );
  const changeAmount = formatCurrency(
    payment ? payment.get("changeAmount") : 0
  );
  const chargesDescription = payment ? payment.get("chargesDescription") : "";
  const chargesAmount = formatCurrency(
    payment ? payment.get("chargesAmount") : 0
  );

  return (
    <div className="content">
      <p className="label text-center mb-4">
        *This is not an Official Receipt*
      </p>
      {!isSinglePayment && (
        <table className="large-mb">
          <thead className="table-not-repeat">
            <tr>
              <th className="w10">Payment</th>
              <th className="w20">Amount to Pay</th>
              <th className="w10">Charges</th>
              <th className="w20">Tendered</th>
              <th className="w10">Change</th>
              <th className="w10">Due At</th>
              <th className="w20">Received At</th>
            </tr>
          </thead>
          <tbody>
            {hasNoPaymentMade ? (
              <tr>
                <td colSpan="7" className="text-center font-italic">
                  No Payments Made
                </td>
              </tr>
            ) : (
              payments.map(payment => {
                const key = payment.id;
                const paymentMethod = payment.get("method")
                  ? payment.get("method").get("name")
                  : "(To Follow)";
                const paidAmount = formatCurrency(payment.get("paidAmount"));
                const chargesAmount = formatCurrency(
                  payment.get("chargesAmount")
                );
                const tenderedAmount = formatCurrency(
                  payment.get("tenderedAmount")
                );
                const changeAmount = formatCurrency(
                  payment.get("changeAmount")
                );
                const dueAt = payment.get("dueAt");
                const paidAt = payment.get("paidAt");

                return (
                  <tr key={key}>
                    <td className="w10">{paymentMethod}</td>
                    <td className="w20">₱ {paidAmount}</td>
                    <td className="w10">₱ {chargesAmount}</td>
                    <td className="w20">₱ {tenderedAmount}</td>
                    <td className="w10">₱ {changeAmount}</td>
                    <td className="w10">{formatDate(dueAt)}</td>
                    <td className="w20">{formatDate(paidAt)}</td>
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      )}

      <table>
        <thead className="table-not-repeat">
          <tr>
            <th className="w5">SKU</th>
            <th className="w-50">Item Description</th>
            <th className="w10">Price</th>
            <th className="w5 text-center">Qty</th>
            <th className="w15">Discount</th>
            <th className="w10">Subtotal</th>
          </tr>
        </thead>
        <tbody>
          {items.map(item => {
            const key = item.id;
            const product = item.get("stock").get("product");
            const name = product.get("name");
            const sku = product.get("sku") || "";
            const unitPrice = formatCurrency(item.get("unitPrice"));
            const originalPrice = item.get("originalPrice");
            const totalPrice = item.get("totalPrice");
            const quantity = item.get("quantity");
            const discountType = item.get("discountType");
            const notes = item.get("notes");
            const hasDiscount = item.get("discount") !== 0;
            const discount = formatCurrency(
              discountType === "FIXED"
                ? item.get("discount")
                : originalPrice - totalPrice
            );

            return (
              <tr key={key}>
                <td className="w10">{sku}</td>
                <td className="w-50">
                  <div>{name}</div>
                  {notes && (
                    <div className="notes text-pre-wrap mt-2 font-italic">
                      {notes}
                    </div>
                  )}
                </td>
                <td className="w10">₱ {unitPrice}</td>
                <td className="w5 text-center">{quantity}</td>
                <td className="w15">
                  ₱ {discount}{" "}
                  {hasDiscount &&
                    discountType === "PERCENTAGE" &&
                    `(${item.get("discount")} %)`}
                </td>
                <td className="w10">₱ {formatCurrency(totalPrice)}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {notes && (
        <div className="text-pre-wrap notes-in-between my-3">{notes}</div>
      )}
      <table>
        <tfoot className="table-not-repeat">
          <tr>
            <td colSpan="2" className="w-50" />
            <td className="label" colSpan="3">
              Overall Discount
            </td>
            <td>₱ {discount}</td>
          </tr>
          <tr>
            <td colSpan="2" />
            <td className="label font-weight-bold" colSpan="3">
              Amount to Pay
            </td>
            <td>₱ {totalPriceFormatted}</td>
          </tr>
          {isSinglePayment && (
            <Fragment>
              {chargesDescription && (
                <tr>
                  <td colSpan="2" />
                  <td className="label" colSpan="3">
                    {chargesDescription}
                  </td>
                  <td>₱ {chargesAmount}</td>
                </tr>
              )}
              <tr>
                <td colSpan="2" />
                <td className="label" colSpan="3">
                  {paymentMethod}
                </td>
                <td>₱ {tenderedAmount}</td>
              </tr>
              <tr>
                <td colSpan="2" />
                <td className="label" colSpan="3">
                  Change
                </td>
                <td>₱ {changeAmount}</td>
              </tr>
            </Fragment>
          )}
        </tfoot>
      </table>
    </div>
  );
}
