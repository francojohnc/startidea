import React, { Fragment } from "react";

export default function Footer({ transaction }) {
  const remarks = transaction.get("remarks");
  const voidStatus = transaction.get("voidStatus");

  return (
    <div className="footer">
      <p className="text-center font-italic mb-4">
        No return and exchange of items after 7 days from date purchased nor
        items were already opened, used, consumed, or physically damaged.
      </p>
      <div className="clearfix">
        <div className="float-left">
          <div>
            <p className="d-inline-block mr-1">Prepared by: </p>
            <div className="signature-line" />
          </div>
          <div className="mt-3">
            <p className="d-inline-block mr-1">Delivered by: </p>
            <div className="signature-line" />
          </div>
        </div>
        <div className="float-right">
          <div>
            <p>Received the above items in good order and condition by:</p>
            <div className="d-flex justify-content-center mt-3">
              <p className="signature-underline">Signature Over Printed Name</p>
            </div>
          </div>
        </div>
      </div>
      {remarks && (
        <Fragment>
          <h5>{voidStatus} Remarks</h5>
          <p className="note text-pre-wrap">{remarks}</p>
        </Fragment>
      )}
    </div>
  );
}
