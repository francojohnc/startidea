import React from "react";
import defaultLogo from "assets/img/copier.png";
import { isValidDate, toDate } from "lib/utils/date";
import formatDate from "lib/utils/formatDate";

const formatDateTime = date => {
  return isValidDate(date)
    ? toDate(date).toLocaleDateString("en-US", {
        month: "short",
        day: "numeric",
        year: "numeric",
        hour12: true,
        hour: "numeric",
        minute: "2-digit"
      })
    : "";
};

export default function Header({ store, transaction, logo = defaultLogo }) {
  const storeLogo = store.get("logo");
  const cashier = transaction.get("cashier");
  const customer = transaction.get("customer");
  const paymentMode = transaction.get("paymentMode").get("name");
  const expectedPickUpAt = transaction.get("expectedPickUpAt");
  const actualPickUpAt = transaction.get("actualPickUpAt");
  const pickUpBy = transaction.get("pickUpBy");
  const isSinglePayment = paymentMode.toLowerCase() === "one-time payment";
  const isPreOrder = paymentMode.toLowerCase() === "pre-order";

  return (
    <div className="header">
      <div className="header--left">
        {logo && (
          <div className="logo__wrapper">
            <img src={storeLogo || logo} alt="Logo" />
          </div>
        )}
        <div className="company">
          {/* <h4>Copieronline Philippines Inc.</h4> */}
          {store.get("name") && <p>{store.get("name")}</p>}
          {store.get("address") && <p>{store.get("address")}</p>}
          {store.get("phone") && <p>{store.get("phone")}</p>}
        </div>
        {customer && (
          <div className="recipient">
            <h4 className="sub-label">Billed To</h4>
            {customer.get("companyName") && (
              <p>{customer.get("companyName")}</p>
            )}
            {customer.get("contactPerson") && (
              <p>{customer.get("contactPerson")}</p>
            )}
            {customer.get("email") && <p>{customer.get("email")}</p>}
            {customer.get("phone") && <p>{customer.get("phone")}</p>}
            {customer.get("address") && <p>{customer.get("address")}</p>}
          </div>
        )}
      </div>
      <div className="header--right">
        <h4 className="heading text-uppercase">Invoice</h4>
        <h4 style={{ textTransform: "unset" }}>#{transaction.id}</h4>
        {!isSinglePayment && (
          <h5 style={{ textTransform: "unset" }}>{paymentMode}</h5>
        )}
        <p className="text-uppercase">
          created at: {formatDateTime(transaction.createdAt)}
        </p>
        {cashier && (
          <p className="text-uppercase">
            created by: {cashier.get("firstName")}
          </p>
        )}
        {isPreOrder && isValidDate(expectedPickUpAt) && (
          <p className="text-uppercase">
            expected pickup at: {formatDate(expectedPickUpAt)}
          </p>
        )}
        {isPreOrder && isValidDate(actualPickUpAt) && (
          <p className="text-uppercase">
            actual pickup at: {formatDateTime(actualPickUpAt)}
          </p>
        )}
        {isPreOrder && !!pickUpBy && (
          <p className="text-uppercase">picked up by: {pickUpBy}</p>
        )}
      </div>
    </div>
  );
}
