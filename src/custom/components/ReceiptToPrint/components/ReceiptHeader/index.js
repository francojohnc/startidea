import React, { Component, Fragment } from "react";

export default class ReceiptHeader extends Component {
  render() {
    return (
      <Fragment>
        <div className="text-center">
          <h2 className="font-sm company-title d-block">
            Copieronline Philippines Inc
          </h2>
          <h5 className="company-address d-block font-weight-normal">
            #45 Kamias Road Brgy Pinyahan Quezon City
          </h5>
          <h5 className="company-contact d-block font-weight-normal">
            927-5892
          </h5>
        </div>
        <div className="text-right">
          <h5 className="receipt-id d-block mb-1">TR# 026658</h5>
          <h5 className="cashier-name d-block font-weight-normal">
            <span>Employee: </span>
            <span>Jerome de Leon</span>
          </h5>
        </div>
      </Fragment>
    );
  }
}
