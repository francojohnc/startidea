import React, { Component, forwardRef, createRef } from "react";
import Button from "components/Button";
import validateFiles from "lib/dom/validateFiles";
import "./styles.min.css";
import Icon from "components/Icon";

const WHITE_LIST_FILE = "image/jpg,image/jpeg,image/png";

const HiddenInputFile = forwardRef(function hiddenInputFile({ onChange }, ref) {
  return (
    <input
      className="hidden-input-style"
      type="file"
      onChange={onChange}
      accept={WHITE_LIST_FILE}
      ref={ref}
    />
  );
});

const defaultProps = {
  base64: false,
  onChange: () => {}
};

export default class ImagePicker extends Component {
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.inputFileRef = createRef();
    this.handlePhotoSelect = this.handlePhotoSelect.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handlePhotoRemove = this.handlePhotoRemove.bind(this);
  }

  handlePhotoSelect() {
    this.inputFileRef.current.click();
  }

  async handlePhotoChange(evt) {
    this.props.onChange(
      (await validateFiles(evt, {
        isBase64: this.props.base64,
        whiteList: WHITE_LIST_FILE
      })).acceptedFiles[0]
    );
  }

  handlePhotoRemove() {
    // reset input file
    this.inputFileRef.current.value = "";
    this.props.onChange(null);
  }

  render() {
    const { src } = this.props;

    return (
      <div className="image-uploader">
        <HiddenInputFile
          onChange={this.handlePhotoChange}
          ref={this.inputFileRef}
        />
        {src ? (
          <div className="display-zone">
            <img src={src} alt="preview" />
            <Button
              className="remove-btn"
              variant="danger"
              onClick={this.handlePhotoRemove}
            >
              <Icon name="times" />
            </Button>
          </div>
        ) : (
          <div className="drop-zone">
            <Button
              className="upload-btn"
              variant="primary"
              onClick={this.handlePhotoSelect}
              isBlock
            >
              Upload Photo
            </Button>
          </div>
        )}
      </div>
    );
  }
}
