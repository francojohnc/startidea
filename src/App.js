import React from "react";
import Parse from "parse";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AppProvider } from "AppContext";
import SignInPage from "screens/auth/SignInPage";
import SignUpScreen from "screens/auth/signup/SignUpScreen";
import MainScreen from "screens/MainPage";

import "./style.min.css";

//parse setup
const PARSE_APP_ID = "e3af844bec24c1ad975700177b47993c";
Parse._initialize(PARSE_APP_ID);
// Parse.serverURL = "http://localhost:8888/v1";
Parse.serverURL = "https://api.startidea.co/v1";

class App extends React.Component {
  render() {
    return (
      <AppProvider>
        <Router>
          <Switch>
            <Route path="/signin" component={SignInPage} />
            <Route path="/signup" component={SignUpScreen} />
            <Route path="/" component={MainScreen} />
          </Switch>
        </Router>
      </AppProvider>
    );
  }
}

export default App;
